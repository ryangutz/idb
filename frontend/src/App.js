import React, { Component } from "react";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Link, Switch, Route } from 'react-router-dom';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import Home from './Home';
import Causes from './Causes';
import Locations from './Locations';
import About from './About';
import Diseases from './Diseases';
import CausesInstance from "./Causes/CausesInstance/CausesInstance";
import DiseaseInstance from "./Diseases/DiseaseInstance";
import LocationInstance from "./Locations/LocationInstance";
import SearchPage from "./Search";
import Visual from './Visualizations/index';

class App extends Component{
  state = {
    search: ""
  };

  handleChange = e => {
    this.setState({
      search: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const search = this.state.search;
    if (search.trim().length === 0) {
      alert("You must enter something!");
      this.setState({ search: "" });
    } else {
      const url = `/search?search=${search}`;
      window.location = url;
    }
  };

  render() {
    return (
      <div>
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="/" className="title">The Health Odyssey</Navbar.Brand>
          <Nav className="ml-auto justify-content-between">
            <Nav.Link href="/Diseases">Diseases</Nav.Link>
            <Nav.Link href="/Locations">Locations</Nav.Link>
            <Nav.Link href="/Causes">Causes</Nav.Link>
            <Nav.Link href="/About">About</Nav.Link>
            <Nav.Link href="/Visualizations">Visualizations</Nav.Link>
            <form class="form-inline my-2 my-lg-0" onSubmit={this.handleSubmit}>
              <input ivalue={this.state.search} onChange={this.handleChange} class="form-control mr-sm-2" type="search" placeholder="" aria-label="Search"/>
              <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
            </form>
          </Nav>
        </Navbar>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/Causes" component={Causes} />
            <Route exact path="/Locations" component={Locations} />
            <Route exact path="/About" component={About} />
            <Route exact path="/Diseases" component={Diseases} />
            <Route exact path="/Diseases/:id" component={DiseaseInstance} />
            <Route exact path="/Causes/:id" component={CausesInstance} />
            <Route exact path="/Locations/:id" component={LocationInstance} />
            <Route exact path="/search" component={SearchPage} />
            <Route exact path="/Visualizations" component={Visual} />
          </Switch>
        </BrowserRouter>
        <footer class="py-5 bg-dark">
          <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; The Health Odyssey 2019</p>
          </div>
        </footer>
      </div>
    );
  }
}

export default App;
