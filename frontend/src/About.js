import React from 'react';
import './About.css';
import chase from './img/chase.jpg';
import ryan from './img/ryan.png';
import megan from './img/megan.jpg';
import tae from './img/tae.jpg';
import simon from './img/simon.jpg';
import slack from './img/slack.jpg';
import flask from './img/flask.png';
import gitlab from './img/gitlab.png';
import mysql from './img/mysql.png';
import postman from './img/postman.jpg';
import react from './img/react.png';
import alchemy from './img/alchemy.jpg';
import mocha from './img/mocha.png';
import selenium from './img/selenium.jpg';
import d3 from './img/d3.jpg';
import reactd3 from './img/reactd3.PNG';
import who from './img/WHO.jpg';
import wiki from './img/wiki.png';
import restcountry from './img/restcountries.png';
import 'bootstrap/dist/css/bootstrap.min.css';

// Data Information Cards Class
class Data extends React.Component {
  render() {
    return (
		  <div className='column'>
        <a href = {this.props.href}>
          <div className="container">
            <img
              src = {this.props.img}
              height='512'
              width='512'
              className="imgabout"
              alt = 'Data Info Images'

            />
            <p><large>{this.props.bio}</large></p>
          </div>
        </a>
		  </div>
    );
	  }
}

// Tool Cards Class
class Tools extends React.Component {
  render() {
    return (
		  <div className='column'>
        <a href = {this.props.href}>
          <div className='card'>
            <div className="container">
              <img
                src = {this.props.img}
                height='512'
                width='512'
                className="imgabout"
                alt = 'Tool Images'

              />
              <p><large>{this.props.bio}</large></p>
            </div>
          </div>
        </a>
		  </div>
    );
	  }
}


// Class of Cumulative
class Cumulative extends React.Component {
  render() {
    return (
		 // <div class='column'>
      <div className='card'>
			  <div className="container">
          <h2 className='card-title'>{this.props.name}</h2>
          <p><large>{this.props.bio}</large></p>

          <div className='card-text'>
				  Number of Issues: {this.props.issues}
          </div>
          <p>
          </p>

          <div className='card-text'>
				  Number of Commits: {this.props.commits}
          </div>
          <p>
          </p>

          <div className='card-text'>
				  Number of Tests: {this.props.tests}
          </div>
          <p>
          </p>
			  </div>

      </div>
    );
	  }
}

// Class of person
class Developer extends React.Component {
  render() {
	  return (
		  <div className='card'>
        <div className="container">
          <div className="row">
            <div className = "col-md-6">
              <h2 className='card-title'>{this.props.name}</h2>
              <div className='card-text'>
							Number of Issues: {this.props.issues}
              </div>
              <p>
              </p>

              <div className='card-text'>
							Number of Commits: {this.props.commits}
              </div>
              <p>
              </p>

              <div className='card-text'>
							Number of Tests: {this.props.tests}
              </div>
              <p>
              </p>
              <p>
							Role: {this.props.role}
              </p>
            </div>
            <div className = "col-md-6">
              <img
                src = {this.props.img}
							 height='225'
							 width='225'
							 className="imgabout"
							 alt = 'Developer Headshots'

              />
            </div>
          </div>
        </div>

		  </div>
	  );
  }
}

// create about to store each person's data
class About extends React.Component {
  constructor(props) {
	  super(props);

	  this.state = {};
	  this.state.totals ={
      'Total Gitlab Data': {
        name: 'Cumulative',
        commits: 0,
        issues: 0,
        tests: 40,
      },
	  };
	  this.state.developerData = {

      'Ryan Gutierrez': {
		  name: 'Ryan Gutierrez',
		  img: ryan,
		  gitlabID: ['Ryan Gutierrez'],
		  role: 'Frontend, Team Lead',
		  commits: 0,
		  issues: 0,
		  tests: 10,
      },
      'Chase Heath': {
		  name: 'Chase Heath',
		  img: chase,
		  gitlabID: ['Chase Heath'],
		  role: 'Fullstack',
		  commits: 0,
		  issues: 0,
		  tests: 0,
      },
      'Simon Pinochet': {
		  name: 'Simon Pinochet',
		  img: simon,
		  gitlabID: ['Simon Pinochet', 'Simon Pinochet Concha'],
		  role: 'Backend',
		  commits: 0,
		  issues: 0,
		  tests: 10,
      },
      'Megan Mealey': {
		  name: 'Megan Mealey',
		  img: megan,
		  gitlabID: ['Megan Mealey'],
		  role: 'Fullstack',
		  commits: 0,
		  issues: 0,
		  tests: 10,
      },
      'Tae Kim': {
		  name: 'Tae Kim',
		  img: tae,
		  gitlabID: ['Taehyoung Kim'],
		  role: 'Frontend',
		  commits: 0,
		  issues: 0,
		  tests: 10,
      },
	  };
	  this.state.tools = {
		  'Slack': {
			  img: slack,
			  bio: 'Slack',
			  href: 'https://slack.com/',
		  },
		  'Flask': {
			  img: flask,
			  bio: 'Flask',
			  href: 'http://flask.palletsprojects.com/',
		  },
		  'Gitlab': {
			  img: gitlab,
			  bio: 'Gitlab',
			  href: 'https://gitlab.com/',
		  },
		  'MySQL': {
			  img: mysql,
			  bio: 'MySQL',
			  href: 'https://www.mysql.com/',
		  },
		  'Postman': {
			  img: postman,
			  bio: 'Postman',
			  href: 'https://www.getpostman.com/',
		  },
		  'React': {
			  img: react,
			  bio: 'React',
			  href: 'https://reactjs.org/',
		  },
		  'SQL Alchemy': {
			  img: alchemy,
			  bio: 'SQL Alchemy',
			  href: 'https://www.sqlalchemy.org/',
		  },
		  'Mocha': {
			  img: mocha,
			  bio: 'Mocha',
			  href: 'https://mochajs.org/',
		  },
		  'Selenium': {
			  img: selenium,
			  bio: 'Selenium',
			  href: 'https://www.seleniumhq.org/',
      },
      'D3': {
        img: d3,
			  bio: 'D3',
			  href: 'https://d3js.org/',
      },
      'react-bubble-chart-d3':{
        img: reactd3,
        bio: '@weknow/react-bubble-chart-d3',
        href: 'https://www.npmjs.com/package/@weknow/react-bubble-chart-d3'
      }
	  };
	  this.state.datas = {
		  'WHO': {
			  img: who,
			  bio: 'WHO API',
			  href: 'https://www.who.int/uat-portal/info/ghoodata-api',
		  },
		  'Wikidata': {
			  img: wiki,
			  bio: 'Wikidata API',
			  href: 'https://wikidata.org/wiki/Wikidata:Data_access',
		  },
		  'Rest Countries': {
			  img: restcountry,
			  bio: 'Rest Countries API',
			  href: 'https://restcountries.eu',
		  },
	 };
  }


  // Fetch data from gitlab api
  // calculate the total number of commits/ issues for each contributor
  componentDidMount() {
    document.title = 'About';
    const mystate = this.state;
    let page = 0;
    while (page < 4) {
      page += 1;
      fetch(
		  'https://gitlab.com/api/v4/projects/14551859/repository/commits?per_page=100&page=' + page,
      )
		  .then((response) => response.json())
		  .then((data) => {
            // process the data
            for (const i in data) {
			  const commit_data = data[i];
			  for (const dev in this.state.developerData) {
                if (mystate.developerData[dev].gitlabID.includes(commit_data.author_name)) {
				  mystate.developerData[dev].commits += 1;
                }
			  }
			  mystate.totals['Total Gitlab Data'].commits += 1;
            }
            this.setState({});
		  })
		  .catch((e) => {
            console.log('Error');
            console.log(e);
		  });
    }


    fetch('https://gitlab.com/api/v4/projects/14551859/issues?per_page=100&page=1')
	  .then((issues) => issues.json())
	  .then((issues) => {
          for (const j in issues) {
		  const data = issues[j];
		  for (const dev in this.state.developerData) {
              if (mystate.developerData[dev].gitlabID.includes(data.author.name)) {
			  mystate.developerData[dev].issues += 1;
              }
		  }
		  mystate.totals['Total Gitlab Data'].issues += 1;
          }
          this.setState({});
	  })
	  .catch((e) => {
          console.log('Error');
          console.log(e);
	  });
  }

  // Rendering all of the updated commits and issues for each developer
  render() {
	  const cards = [];
	  const totalCard = [];
	  const toolCards = [];
	  const dataCards = [];
    for (const dev in this.state.developerData) {
      cards.push(
          <div id={dev} className='col-md-6 mt-4'>
            <Developer {...this.state.developerData[dev]} />
          </div>,
      );
	  }
	  console.log(cards);
	  totalCard.push(
        <div id={'Total Gitlab Data'} className='col-md-6 mt-4'>
          <Cumulative {...this.state.totals['Total Gitlab Data']} />
        </div>,
    );
    for (const tool in this.state.tools) {
      toolCards.push(
          <div id={tool} className='col-md-4 mt-4'>
            <Tools {...this.state.tools[tool]} />
          </div>,
      );
    }
    for (const data in this.state.datas) {
      dataCards.push(
          <div id={data} className='col-md-4 mt-4 card'>
            <Data {...this.state.datas[data]} />
          </div>,
      );
    }
	  // Display Results
	  return (
      <div className='main mb-4'>
        <h2 className="mt-3 justify-text-center">About Page</h2>

			 {/* display group developers */}
		  <div className='container'>
          <h1><large>Our goal with this website</large></h1>
          <p>We wanted to help people who wanted to travel the dangers that could come with travelling. Our website offers a list
				of the most prominent diseases in multiple countries and other information about them such as prevention and a count
				of the diseases in the area.
          </p>

          <div className='row justify-content-center'>
            <div className='row'><h2> Developers</h2></div>
            <div className = 'row justify-content-center'>{cards}</div>
          </div>
          <div className='row justify-content-center mt-4'>
            {totalCard}
          </div>
          <div className='row justify-content-center mt-4'>
            <div className='row'><h2> Tools we used</h2></div>
            <div className = 'row'>{toolCards}</div>
          </div>
          <div className='container justify-content-center mt-4'>
            <div className='row justify-content-center'><h2> Data sources we used</h2></div>
            <div className = 'row'>{dataCards}</div>
          </div>
		  </div>

		  <div className="justify-content-center">
          <h2>Links to our code</h2>
          <div className = "text-center">
            <div className = "mb-3"><a href="https://www.gitlab.com/ryangutz/idb" className="btn btn-secondary btn-lg active justify-content-center" role="button" aria-pressed="true">GitLab Repo</a></div>
            <div><a href="https://documenter.getpostman.com/view/8975560/SW18vueB?version=latest" className="btn btn-secondary btn-lg active" role="button" aria-pressed="true">API</a></div>
		  		</div>
		  </div>
      </div>
	  );
  }
}

export default About;
