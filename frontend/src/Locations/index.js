import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Pagination from '../Pagination';
import './Location.css';
import Filter from './filterIndex';
import Highlighter from 'react-highlight-words';

class Locations extends Component {
  constructor() {
    super();
    this.state = {
      locations: [],
      currPage: 1,
      totalPages: 1,
      queryString: '&',
    };
    this.filtering = this.filtering.bind(this);
    this.searching = this.searching.bind(this);
  }

  filtering() {
    let queryString = '';
    const filterRegion = document.getElementById('region');
    const filterPop = document.getElementById('population');
    const signPop = document.getElementById('pop');
    const filterMR = document.getElementById('mortality_rate');
    const signMR = document.getElementById('mort');
    const filterHosp = document.getElementById('hospitals_per_100k');
    const signH = document.getElementById('hosp');
    const filterPrevD = document.getElementById('preventable_deaths');
    const signPD = document.getElementById('pd');
    const sort = document.getElementById('sort');
    const attr = document.getElementById('attribute');
    if (filterRegion.selectedIndex !== 0) {
      queryString += '&region=' + filterRegion.options[filterRegion.selectedIndex].value;
    }

    if (sort.value && attr.value) {
      queryString += '&sort=' + attr.value + sort.value;
    }
    console.log(queryString);
    fetch('https://api.thehealthodyssey.com/api/locations?results_per_page=10' + queryString)
        .then((response) => response.json()).then((json) => {
          const locations = json['objects'];
          const curr = json['page'];
          const total = json['total_pages'];
          this.setState({locations: locations, currPage: curr, totalPages: total, queryString: queryString});
        });
  }

  searching() {
    let queryString = '';
    const searchLoc = document.getElementById('search_all');
    if (searchLoc.value) {
      queryString += searchLoc.value;
    }
    fetch('https://api.thehealthodyssey.com/api/locations?search=' + queryString)
        .then((response) => response.json()).then((json) => {
          const locations = json['objects'];
          const curr = json['page'];
          const total = json['total_pages'];
          this.setState({locations: locations, currPage: curr, totalPages: total, queryString: queryString});
        });
  }

  componentDidMount() {
    const querystring = this.props.location.search;
    let page = 1;
    let qs = '';
    if (querystring !== '') {
      const parsedQuerystring = querystring.substring(1);
      const queries = parsedQuerystring.split('&');
      queries.forEach((query) => {
        const keyValue = query.split('=');
        if (keyValue.length > 1 && keyValue[0] === 'page') {
          page = keyValue[1];
        } else if (keyValue.length > 1 ) {
          qs += '&'+query;
        }
      });
    }

    fetch('https://api.thehealthodyssey.com/api/locations?results_per_page=10&page=' + page + qs)
        .then((response) => response.json()).then((json) => {
          const locations = json['objects'];
          const curr = json['page'];
          const total = json['total_pages'];
          this.setState({locations: locations, currPage: curr, totalPages: total, queryString: qs});
        });
  }

  render() {
    const renderedLocations = this.state.locations.map((d) => {
      return (
        <tr>
          <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.queryString]} autoEscape={true} textToHighlight={d.name}/></td>
          <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.queryString]} autoEscape={true} textToHighlight={d.region}/></td>
          <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.queryString]} autoEscape={true} textToHighlight={'' + d.population}/></td>
          <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.queryString]} autoEscape={true} textToHighlight={'' + d.mortality_rate}/></td>
          <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.queryString]} autoEscape={true} textToHighlight={'' + d.hospitals_per_100k}/></td>
          <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.queryString]} autoEscape={true} textToHighlight={'' + d.preventable_deaths}/></td>
          <Link
            to={{
              pathname: `/Locations/${d.name}`,
            }}
          >See More</Link>
        </tr>
      );
    });

    return (
      <div className = "container">
        <div className="form-inline col-lg-12 search">
          <input className="form-control col-lg-10" type="text"name="search_all"id="search_all"></input>
          <button className="btn btn-secondary col-lg-2" onClick={this.searching} type="submit">Search Locations</button>
        </div>
        <Filter />
        <div className="text-center">
          <button className="btn btn-secondary" onClick={this.filtering}>Apply Filters</button>
        </div>
        <div className="title">Search by specific location</div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Region</th>
              <th scope="col">Population</th>
              <th scope="col">Mortality Rate</th>
              <th scope="col">Hospitals per 100,000</th>
              <th scope="col">Preventable Deaths</th>
            </tr>
          </thead>
          <tbody>
            {renderedLocations}
          </tbody>
        </table>
        <Pagination
          url="/Locations"
          query = {this.state.queryString}
          current={this.state.currPage}
          lastPage={this.state.totalPages}
        />
      </div>
    );
  }
}

export default Locations;
