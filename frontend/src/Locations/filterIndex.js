import * as React from 'react';
import classnames from 'classnames';

import styles from './styles.module.css';


class Filter extends React.Component {
  render() {
    const containerClasses = classnames('container', 'mb-1', styles.container);
    const formClasses = classnames('form-horizontal', styles.form);

    return (
      <div>
        <div className={containerClasses}>
          <form className={formClasses} noValidate>
            <p className="mb-1">Refine your results</p>
            <div className="row">
              <div className="row col-8">
                <div className="column col-3 col-xs-12">
                  <div className="form-group">
                    <div className="col-3 col-sm-12">
                      <label className="form-label" htmlFor="region">
                      Region
                      </label>
                    </div>
                    <div className="col-9 col-sm-12">
                      <select className="form-select" id="region">
                        <option value="0">Choose...</option>
                        <option value="Africa">Africa</option>
                        <option value="Americas">Americas</option>
                        <option value="Eastern Mediterranean">Eastern Mediterranean</option>
                        <option value="Europe">Europe</option>
                        <option value="South-East Asia">South-East Asia</option>
                        <option value="Western Pacific">Western Pacific</option>
                      </select>
                    </div>
                  </div>
                </div>

              </div>
              <div className="row col-3">
                <div className="column col-6 col-xs-12">
                  <div className="form-group">
                    <div className="col-3 col-sm-12">
                      <label className="form-label" htmlFor="sort">
                      Sorting
                      </label>
                    </div>
                    <div className="col-9 col-sm-12">
                      <select className="form-select" id="sort">
                        <option value="">Choose...</option>
                        <option value=":asc">Ascending</option>
                        <option value=":desc">Descending</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div className="column col-6 col-xs-12">
                  <div className="form-group">
                    <div className="col-3 col-sm-12">
                      <label className="form-label" htmlFor="attribute">
                      Column
                      </label>
                    </div>
                    <div className="col-9 col-sm-12">
                      <select className="form-select" id="attribute">
                        <option value="">Choose...</option>
                        <option value="name">Name</option>
                        <option value="region">Region</option>
                        <option value="population">Population</option>
                        <option value="mortality_rate">Mortality Rate</option>
                        <option value="hospitals_per_100k"> Hospitals Per 100k</option>
                        <option value="preventable_deaths">Preventable Deaths</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default Filter;
