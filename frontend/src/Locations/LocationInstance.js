import React from 'react';
const styles = {
  margin: '50px',
};

// Class for giving each disease/cause a hyperlink

class URL extends React.Component {
  render() {
    return (
      <a href={this.props.url}>{this.props.name}, </a>
    );
  }
}

// Class for providing a hyperlink to the disease/cause model page

class URL_Last extends React.Component {
  render() {
    return (
      <a href={this.props.url}>{this.props.name} </a>
    );
  }
}

class LocationInstance extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      location: {},
      diseases: {},
      causes: {},
    };
    this.addDiseases = this.addDiseases.bind(this);
    this.addCauses = this.addCauses.bind(this);
  }

  componentDidMount() {
    const name = this.props.match.params.id;
    fetch('https://api.thehealthodyssey.com/api/locations/' + name)
        .then((res) => res.json())
        .then((data) => {
          const dis = {};
          const cause = {};
          for (const x in data.diseases) {
            dis[x] = {name: data.diseases[x], url: 'https://www.thehealthodyssey.com/Diseases/' + data.diseases[x]};
          }
          for (const x in data.causes) {
            cause[x] = {name: data.causes[x], url: 'https://www.thehealthodyssey.com/Causes/' + data.causes[x]};
          }
          this.setState({
            location: data,
            diseases: dis,
            causes: cause,
          });
        });
  }

  // Adds all the diseases in a location to a class to add hyperlinks to each of them to their respective page

  addDiseases() {
    const dis = [];
    let x = 0;
    for (x in this.state.diseases) {
      dis.push(
          <URL {...this.state.diseases[x]} />,
      );
    }
    dis.pop();
    dis.push(
        <URL_Last {...this.state.diseases[x]} />,
    );
 
    return dis;
  }

  // Adds all the causes in a location to a class to add hyperlinks to each of them to their respective page

  addCauses() {
    const cause = [];
    let x = 0;
    for (x in this.state.causes) {
      cause.push(
          <URL {...this.state.causes[x]} />,
      );
    }
    cause.pop();
    cause.push(
        <URL_Last {...this.state.causes[x]} />,
    );
    return cause;
  }

  render() {
    const diseases = this.addDiseases();
    const causes = this.addCauses();

    return (
      <div className ="container">
        
        <h2>{this.state.location.name}</h2>
        <h2><img src={this.state.location.flag} class="rounded" alt="..."></img></h2>
        <table className="table table-striped" style={styles}>
          <thead>
            <tr>
              <th scope="col">Attribute</th>
              <th scope="col">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">Region</th>
              <td>{this.state.location.region}</td>
            </tr>
            <tr>
              <th scope="row">Population</th>
              <td>{this.state.location.population}</td>
            </tr>
            <tr>
              <th scope="row">Mortality Rate</th>
              <td>{this.state.location.mortality_rate}</td>
            </tr>
            <tr>
              <th scope="row">Preventable Deaths</th>
              <td>{this.state.location.preventable_deaths}</td>
            </tr>
            <tr>
              <th scope="row">Service Coverage Index</th>
              <td>{this.state.location.service_coverage_index}</td>
            </tr>
            <tr>
              <th scope="row">Diseases</th>
              <td>
                {diseases}
              </td>
            </tr>
            <tr>
              <th scope="row">Causes</th>
              <td>
                {causes}
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default LocationInstance;
