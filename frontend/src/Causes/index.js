import React from 'react';
import './Causes.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Link} from 'react-router-dom';
import Pagination from '../Pagination';
import Filter from './filterIndex';
import Highlighter from 'react-highlight-words';

class Causes extends React.Component {
  constructor() {
    super();
    this.state = {
      currPage: 1,
      causes: [],
      totalPages: 1,
      queryString: '',
    };
    this.filtering = this.filtering.bind(this);
    this.searching = this.searching.bind(this);
  }

  filtering() {
    let queryString = '';
    const filterTrans = document.getElementById('search_transmission');
    const filterTreat = document.getElementById('search_treatment');
    const sort = document.getElementById('sort');
    const attr = document.getElementById('attribute');
    if (filterTrans.value) {
      queryString += '&transmission=' + filterTrans.value;
    }
    if (filterTreat.value) {
      queryString += '&treatment=' + filterTreat.value;
    }
    if (sort.value && attr.value) {
      queryString += '&sort=' + attr.value + sort.value;
    }
    fetch('https://api.thehealthodyssey.com/api/causes?results_per_page=10' + queryString)
        .then((response) => response.json()).then((json) => {
          const causes = json['objects'];
          const curr = json['page'];
          const total = json['total_pages'];
          this.setState({causes: causes, currPage: curr, totalPages: total, queryString: queryString});
          console.log(queryString);
        });
  }

  searching() {
    let queryString = '';
    const searchCauses = document.getElementById('search_all');
    if (searchCauses.value) {
      queryString += searchCauses.value;
    }
    fetch('https://api.thehealthodyssey.com/api/causes?search=' + queryString)
        .then((response) => response.json()).then((json) => {
          const causes = json['objects'];
          const curr = json['page'];
          const total = json['total_pages'];
          this.setState({causes: causes, currPage: curr, totalPages: total, queryString: queryString});
        });
  }

  componentDidMount() {
    const querystring = this.props.location.search;
    let page = 1;
    let qs = '';
    if (querystring !== '') {
      const parsedQuerystring = querystring.substring(1);
      const queries = parsedQuerystring.split('&');
      queries.forEach((query) => {
        const keyValue = query.split('=');
        if (keyValue.length > 1 && keyValue[0] === 'page') {
          page = keyValue[1];
        } else if (keyValue.length > 1) {
          qs += '&'+query;
        }
      });
    }

    const url = 'https://api.thehealthodyssey.com/api/causes?results_per_page=10&page=' + page + qs;
    fetch(url)
        .then((response) => response.json()).then((json) => {
          const causes = json['objects'];
          const curpage = json['page'];
          const lastpage = json['total_pages'];
          this.setState({causes: causes, currPage: curpage, totalPages: lastpage, queryString: qs});
        });
  }
  render() {
    const renderCauses = this.state.causes.map((c, i) => {
      return (
        <tr>
          <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.queryString]} autoEscape={true} textToHighlight={c.name}/></td>
          <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.queryString]} autoEscape={true} textToHighlight={c.disease.join(', ')}/></td>
          <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.queryString]} autoEscape={true} textToHighlight={c.reaction_time}/></td>
          <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.queryString]} autoEscape={true} textToHighlight={c.transmission.join(', ')}/></td>
          <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.queryString]} autoEscape={true} textToHighlight={c.treatment.join(', ')}/></td>
          <Link
            to={{
              pathname: `/Causes/${c.name}`,
            }}
          >See More</Link>
        </tr>
      );
    });

    return (
      <div className = "container">
        <div className="form-inline col-lg-12 search">
          <input className="form-control col-lg-10" type="text"name="search_all"id="search_all"></input>
          <button className="btn btn-secondary col-lg-2" onClick={this.searching} type="submit">Search Causes</button>
        </div>
        <Filter />
        <div className="text-center">
          <button className="btn btn-secondary" onClick={this.filtering}>Apply Filters</button>
        </div>
        <div className="title">Search by a specific cause</div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Disease</th>
              <th scope="col">Reaction Time</th>
              <th scope="col">Transmission</th>
              <th scope="col">Treatment</th>
            </tr>
          </thead>
          <tbody>
            {renderCauses}
          </tbody>
        </table>
        <Pagination
          url="/Causes"
          query ={this.state.queryString}
          current={this.state.currPage}
          lastPage={this.state.totalPages}
        />
      </div>
    );
  }
}

export default Causes;
