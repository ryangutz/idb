import React from 'react';
import './Causes.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const styles = {
  height: '15rem',
  padding: '50px',
};


function Cause(props) {
  return (
    <div className="card-group">
      <div className="card" >
        <div className="card-body">
          <h5 className="card-title">{props.name}</h5>
          <div className="Cause"><h1>About: {props.about}</h1></div>
          <div className="Cause"><h1>Disease: {props.disease}</h1></div>
          <div className="Cause"><h1>Location: {props.location}</h1></div>
          <div className="Cause"><h1>Maximum Incubation Period(days): {props.maxincubation}</h1></div>
          <div className="Cause"><h1>Mortality Rate: {props.potency}</h1></div>
        </div>
      </div>
    </div>


  );
}

export default Cause;


