import './CausesInstance.css';
import React from 'react';
let styles = {
  margin: '50px',
};

class ATTR extends React.Component {
  render() {
    return (
      <span>{this.props.name}, </span>
    )
  } 
}

//Class for giving each location/diseases a hyperlink

class URL extends React.Component {
  render() {
    return (
      <span><a href={this.props.url}>{this.props.name}</a>, </span>
      
    )
  } 
}

//Class for providing a hyperlink to the locations/disease model page

class URL_Link extends React.Component {
  render() {
    return (
      <a href={this.props.url}>{this.props.name} </a>
    )
  } 
}

class CausesInstance extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      locations: {},
      diseases: {},
      cause: {},
      treatments: {},
      transmissions: {}
    }
    this.addLocations = this.addLocations.bind(this);
    this.addDiseases = this.addDiseases.bind(this);
    this.addTreatments = this.addTreatments.bind(this);
    this.addTransmissions = this.addTransmissions.bind(this);
  }

  componentDidMount() {
   const id = this.props.match.params.id;
   fetch("https://api.thehealthodyssey.com/api/causes/" + id)
     .then(res => res.json())
     .then(data => {
        let loc = {};
        let dis = {};
        let treat = {};
        let trans = {};
        for(let x in data.locations)
          loc[x] = {name: data.locations[x], url: "https://www.thehealthodyssey.com/Locations/" + data.locations[x]};
        for(let x in data.disease)
          dis[x] = {name: data.disease[x], url: "https://www.thehealthodyssey.com/Diseases/" + data.disease[x]};
        for(let x in data.treatment)
          treat[x] = {name: data.treatment[x]};
        for(let x in data.transmission)
          trans[x] = {name: data.transmission[x]};

        this.setState({
          locations: loc,
          diseases: dis,
          cause: data,
          treatments: treat,
          transmissions: trans
        });
     });
  }

  //Adds all the locations that have this cause to a class to add hyperlinks to each of them to their respective page

 addLocations() {
  let loc = [];
  let x = 0;
 for(x in this.state.locations){
   loc.push(
       <URL {...this.state.locations[x]} />
   )
 }
 loc.pop()
 loc.push(
   <URL_Link {...this.state.locations[x]} />
 )
 return loc;
}

//Adds all the diseases caused by a cause to a class to add hyperlinks to each of them to their respective page

addDiseases() {
 let dis = [];
 let x = 0;
for(x in this.state.diseases){
  dis.push(
      <URL {...this.state.diseases[x]} />
  )
}
dis.pop()
dis.push(
  <URL_Link {...this.state.diseases[x]} />
)
return dis;

}

addTreatments() {
  let treat = [];
  let x = 0;
 for(x in this.state.treatments){
   treat.push(
       <ATTR {...this.state.treatments[x]} />
   )
 }
 treat.push(
   <URL_Link {...this.state.treatments[x]} />
 )
 return treat;
 }


 addTransmissions() {
  let trans = [];
  let x = 0;
 for(x in this.state.transmissions){
   trans.push(
       <ATTR {...this.state.transmissions[x]} />
   )
 }
 trans.push(
   <URL_Link {...this.state.transmissions[x]} />
 )
 return trans;
 }

  render() {
    let locations = this.addLocations();
    let diseases = this.addDiseases();
    let treatments = this.addTreatments();
    let transmissions = this.addTransmissions();

      return (

          <div class ="container">
          <h2>{this.state.cause.name}</h2>
          <table class="table table-striped" style={styles}>
            <thead>
              <tr>
                <th scope="col">Attribute</th>
                <th scope="col">Description</th>
              </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Disease</th>
                <td>{diseases}</td>
              </tr>
              <tr>
                <th scope="row">Reaction Time</th>
                <td>{this.state.cause.reaction_time}</td>
              </tr>
              <tr>
                <th scope="row">Transmission</th>
                <td>{transmissions}</td>
              </tr>
              <tr>
                <th scope="row">Treatment</th>
                <td>{treatments}</td>
              </tr>
              <tr>
                <th scope="row">Locations</th>
                <td>
                    {locations}
                </td>
              </tr>
            </tbody>
          </table>
          </div>




    );
  }
}

export default CausesInstance;
