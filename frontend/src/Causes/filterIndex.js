import * as React from 'react';
import classnames from 'classnames';

import styles from './styles.module.css';

class Filter extends React.Component {
  render() {
    const containerClasses = classnames('container', 'mb-1', styles.container);
    const formClasses = classnames('form-horizontal', styles.form);

    return (
      <div>
        <div className={containerClasses}>
          <form className={formClasses} noValidate>
            <p className="mb-1">Refine your results</p>
            <div className="row">
              <div className="column col-3 col-xs-12">
                <div className="form-group">
                  <div className="col-3 col-sm-12">
                    <label className="form-label" htmlFor="search_transmission">
                      Transmission
                    </label>
                  </div>
                  <div className="col-9 col-sm-12">
                    <form>
                      <input type="text"name="search_transmission"id="search_transmission"></input>
                    </form>
                  </div>
                </div>
              </div>
              <div className="column col-3 col-xs-12">
                <div className="form-group">
                  <div className="col-3 col-sm-12">
                    <label className="form-label" htmlFor="search_treatment">
                      Treatment
                    </label>
                  </div>
                  <div className="col-9 col-sm-12">
                    <form>
                      <input type="text"name="search_treatment"id="search_treatment"></input>
                    </form>
                  </div>
                </div>
              </div>
              <div className="column col-2 col-xs-12">
                <div className="form-group">
                  <div className="col-3 col-sm-12">
                    <label className="form-label" htmlFor="sort">
                      Sorting
                    </label>
                  </div>
                  <div className="col-3 col-sm-12">
                    <select className="form-select" id="sort">
                      <option value="">Choose...</option>
                      <option value=":asc">Ascending</option>
                      <option value=":desc">Descending</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="column col-2 col-xs-12">
                <div className="form-group">
                  <div className="col-3 col-sm-12">
                    <label className="form-label" htmlFor="attribute">
                      Column
                    </label>
                  </div>
                  <div className="col-3 col-sm-12">
                    <select className="form-select" id="attribute">
                      <option value="">Choose...</option>
                      <option value="name">Name</option>
                      {/* <option value="transmission">Transmission</option>
                      <option value="treatment">Treatment</option> */}
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default Filter;
