import React, { Component } from "react";
import { Link } from 'react-router-dom';
import Highlighter from "react-highlight-words";

class SearchPage extends Component {
  state = {
    collapse: {
      diseases: true,
      causes: true,
      locations: true
    },
    keyword : null,
    diseases: [],
    causes: [],
    locations: [],
    loading: true,
    currentPage: 1,
    numPages: 0
  };

  handleClick = e => {
    let collapse = this.state.collapse;
    collapse[e.target.name] = !collapse[e.target.name];
    this.setState({ collapse });
  };

  componentDidMount() {
    const querystring = this.props.location.search;
    const keyword = querystring.substring(1).split("=")[1];
    this.setState({keyword: keyword});
    const url = `https://api.thehealthodyssey.com/api/`;
    fetch(url + "causes" + querystring)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        this.setState({
          causes: data['objects'],
          loading: false
        });
      })
      .catch(console.log);

      fetch(url + "diseases" + querystring)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        this.setState({
          diseases: data['objects'],
          loading: false
        });
      })
      .catch(console.log);

      fetch(url + "locations" + querystring)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        this.setState({
            locations: data['objects'],
          loading: false
        });
      })
      .catch(console.log);
  }

  render() {
    if (this.state.loading) {
      return <h2 className="text-center">Loading...</h2>;
    }
    let diseases = this.state.diseases.map((d) => {
        return (
            <tr>
              <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.keyword]} autoEscape={true} textToHighlight={d.name}/></td> 
              <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.keyword]} autoEscape={true} textToHighlight={d.highest_concentrated_region}/></td> 
              <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.keyword]} autoEscape={true} textToHighlight={d.locations.join(", ")}/></td> 
              <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.keyword]} autoEscape={true} textToHighlight={d.causes.join(", ")}/></td> 
              <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.keyword]} autoEscape={true} textToHighlight={d.symptoms.join(", ")}/></td>
              <Link
                to={{
                  pathname: `/Diseases/${d.name}`,
                }}
              >See More</Link>
          </tr>
        );
      });

      let causes = this.state.causes.map((c) => {
        return (
            <tr>
                <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.keyword]} autoEscape={true} textToHighlight={c.name}/></td>
                <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.keyword]} autoEscape={true} textToHighlight={c.disease.join(", ")}/></td>
                <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.keyword]} autoEscape={true} textToHighlight={c.reaction_time}/></td>
                <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.keyword]} autoEscape={true} textToHighlight={c.transmission.join(", ")}/></td>
                <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.keyword]} autoEscape={true} textToHighlight={c.treatment.join(", ")}/></td> 
            <Link
              to={{
                pathname: `/Causes/${c.name}`,
              }}
            >See More</Link>
        </tr>
        );
      });

      let locations = this.state.locations.map((d) => {
        return (
            <tr>
                <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.keyword]} autoEscape={true} textToHighlight={d.name}/></td>
                <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.keyword]} autoEscape={true} textToHighlight={d.region}/></td>
                <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.keyword]} autoEscape={true} textToHighlight={"" + d.population}/></td>
                <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.keyword]} autoEscape={true} textToHighlight={"" + d.mortality_rate}/></td>
                <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.keyword]} autoEscape={true} textToHighlight={"" + d.hospitals_per_100k}/></td>
                <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.keyword]} autoEscape={true} textToHighlight={"" + d.preventable_deaths}/></td>
            <Link
              to={{
                pathname: `/Locations/${d.name}`,
              }}
            >See More</Link>
        </tr>
        );
      });
    
      return (
          <div class="container">
              <h1>Diseases</h1>
        <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Highest-Concentrated-Region</th>
            <th scope="col">Locations</th>
            <th scope="col">Causes</th>
            <th scope="col">Symptoms</th>
          </tr>
        </thead>
        <tbody>
            {diseases}
                    </tbody>
        </table>
        <tbody>

        <h1>Causes</h1>
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Disease</th>
              <th scope="col">Reaction Time</th>
              <th scope="col">Transmission</th>
              <th scope="col">Treatment</th>
            </tr>

          </thead>
          <tbody>
                {causes}
         </tbody>

          </table>
          <h1>Locations</h1>
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Region</th>
              <th scope="col">Population</th>
              <th scope="col">Mortality Rate</th>
              <th scope="col">Hospitals per 100,000</th>
              <th scope="col">Preventable Deaths</th>
            </tr>

          </thead>
          <tbody>
            {locations}
         </tbody>
          </table>

        </tbody>
        </div>
      )
  }
}

export default SearchPage;