import React from 'react';
import './Home.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import waves from './img/wave.PNG';
import stamp from './img/stamp.png';
import disease from './img/disease.png';
import cause from './img/cause.png';
import locations from './img/locations.png';
class Home extends React.Component {
  render() {
    return (
      <body>
        <div className="container">
          <header className="jumbotron my-4">
            <h1 className="display-3">The Health Odyssey</h1>
          </header>
	  <div className="section-break">
	    <img src={waves} alt="wave banner" className="section-break" />
	  </div>
	  <br/>
	  <br/>
	  <div className="postcard">
	    <div className="row text-center">
	    <div className="col-lg-11">
	    <div className="ps-title">Stay Healthy on your Journey</div>
	    </div>
	    <div className="col-lg-1">
	      <img className="stmp" src={stamp} />
	    </div>
	    </div>
	    <div className="row text-center">
	      <div className="col">
	        <div className="ps-card-l h-100">
	          <div className="card-body">
	            <h4 className="card-title"><strong>Inform yourself about health risks abroad</strong></h4>
	            <p>The Health Odyssey is a tool built to keep tourists safe by collecting and displaying information about communicable diseases worldwide.</p>
	    <p>Our database collects information from global health organizations, such as the World Health Organization, 
	    and aggregates it so anyone can easily see what diseases are spreading in a given area as well as information about 
	    each disease and the pathogen that causes it.</p>
	          </div>
	        </div>
	      </div>
	      <div className="col">
	        <div className="ps-card-r h-100">
	          <div className="card-body">
	            <br/>
	            <br/>
	            <h4 className="card-title">How to use The Health Odyssey</h4>
	            <p>Select a category of data below to see all of the instances cataloged. From there, instances can be searched and filtered.</p>
	            <p>Alternatively, search the entire site from the search bar at the top of the page.</p>
                    <p> <a href="/About">About the Developers</a></p>
	          </div>
	        </div>
	      </div>
	    </div>
	    <br/>
	  </div>
	  <br/>
	  <br/>
	  <br/>
          <div className="model-title-box">
	    <h2>Browse the Database</h2>
 	  </div>
          <div className="row text-center">

            <div className="col-lg-4 col-md-6 mb-4">
              <div className="card h-100">
                <img className="card-img-top" src={disease} alt=""></img>
                <div className="card-body">
                  <h4 className="card-title">Diseases</h4>
	          <p>See which diseases are being tracked as global health risks, and learn about the symptoms and mode of transmission for each.</p>
                  <p><a href="/Diseases" className="btn btn-secondary">Search Diseases</a></p>
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 mb-4">
              <div className="card h-100">
                <img className="card-img-top" src={cause} alt=""></img>
                <div className="card-body">
                  <h4 className="card-title">Causes</h4>
	    	  <p>Causes are the pathogens that cause diseases. Look at the names and associated diseases for each cause as well 
	    	  as the incubation period, possible treatment, and countries it is found in.</p>
                  <p><a href="/Causes" className="btn btn-secondary">Search Causes</a></p>
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 mb-4">
              <div className="card h-100">
                <img className="card-img-top" src={locations} alt=""></img>
                <div className="card-body">
                  <h4 className="card-title">Locations</h4>
	          <p>View the vital statistics for each country, such as the mortality rate and the percentage of deaths attributed to disease. 
	          Each country's profile has information on which diseases have been detected and the number of cases reported.</p>
                  <p> <a href="/Locations" className="btn btn-secondary">Search Locations</a></p>
                </div>
              </div>
            </div>


          </div>

        </div>

        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      </body>

    );
  }
}

export default Home;
