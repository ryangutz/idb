import React from 'react';
import './stylesheet.css';

function Disease(props) {
  return (
    <div className ="container">
      <h2 className = "title">{props.name}</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">Attribute</th>
            <th scope="col">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">Average Treatment Cost</th>
            <td>{props.atc}</td>
          </tr>
          <tr>
            <th scope="row">Origin</th>
            <td>{props.origin}</td>
          </tr>
          <tr>
            <th scope="row">Causes</th>
            <td>{props.causes}</td>
          </tr>
          <tr>
            <th scope="row">Symptoms</th>
            <td>
              {props.symptoms}
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default Disease;
