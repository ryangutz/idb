import * as React from 'react';
import classnames from 'classnames';

import styles from './styles.module.css';

class Filter extends React.Component {
  render() {
    const containerClasses = classnames('container', 'mb-1', styles.container);
    const formClasses = classnames('form-horizontal', styles.form);

    return (
      <div>
        <div className={containerClasses}>
          <form className={formClasses} noValidate>
            <p className="mb-1">Refine your results</p>
            <div className="row">
              <div className="column col-3 col-xs-12">
                <div className="form-group">
                  <div className="col-3 col-sm-12">
                    <label className="form-label" htmlFor="region">
                    Region
                    </label>
                  </div>
                  <div className="col-9 col-sm-12">
                    <select className="form-select" id="highest_concentrated_region">
                      <option value="0">Choose...</option>
                      <option value="Africa">Africa</option>
                      <option value="Americas">Americas</option>
                      <option value="Eastern Mediterranean">Eastern Mediterranean</option>
                      <option value="Europe">Europe</option>
                      <option value="South-East Asia">South-East Asia</option>
                      <option value="Western Pacific">Western Pacific</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="column col-3 col-xs-12">
                <div className="form-group">
                  <div className="col-3 col-sm-12">
                    <label className="form-label" htmlFor="search_location">
                      Location
                    </label>
                  </div>
                  <div className="col-9 col-sm-12">
                    <form>
                      <input type="text"name="search_location"id="search_location"></input>
                    </form>
                  </div>
                </div>
              </div>
              <div className="column col-3 col-xs-12">
                <div className="form-group">
                  <div className="col-3 col-sm-12">
                    <label className="form-label" htmlFor="search_symptom">
                      Symptom
                    </label>
                  </div>
                  <div className="col-9 col-sm-12">
                    <form>
                      <input type="text"name="search_symptom"id="search_symptom"></input>
                    </form>
                  </div>
                </div>
              </div>
              <div className = "row">
                <div className="column col-6 col-xs-12">
                  <div className="form-group">
                    <div className="col-3 col-sm-12">
                      <label className="form-label" htmlFor="sort">
                      Sorting
                      </label>
                    </div>
                    <div className="col-3 col-sm-12">
                      <select className="form-select" id="sort">
                        <option value="">Choose...</option>
                        <option value=":asc">Ascending</option>
                        <option value=":desc">Descending</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div className="column col-6 col-xs-12">
                  <div className="form-group">
                    <div className="col-3 col-sm-12">
                      <label className="form-label" htmlFor="attribute">
                      Column
                      </label>
                    </div>
                    <div className="col-3 col-sm-12">
                      <select className="form-select" id="attribute">
                        <option value="">Choose...</option>
                        <option value="name">Name</option>
                        <option value="highest_concentrated_region">Region</option>
                        {/* <option value="transmission">Transmission</option>
                      <option value="treatment">Treatment</option> */}
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default Filter;
