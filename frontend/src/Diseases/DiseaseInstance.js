import React from 'react';
import './stylesheet.css';
const styles = {
  margin: '50px',
};

class Symptoms extends React.Component {
  render() {
    return (
      <span>{this.props.name}, </span>
    );
  }
}

// Class for giving each location/cause a hyperlink

class URL extends React.Component {
  render() {
    return (
      <span><a href={this.props.url}>{this.props.name}</a>, </span>

    );
  }
}

// Class for providing a hyperlink to the locations/cause model page

class URL_Last extends React.Component {
  render() {
    return (
      <a href={this.props.url}>{this.props.name} </a>
    );
  }
}

class DiseaseInstance extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locations: {},
      disease: {},
      causes: {},
      symptoms: {},
    };
    this.addLocations = this.addLocations.bind(this);
    this.addCauses = this.addCauses.bind(this);
    this.addSymptoms = this.addSymptoms.bind(this);
  }

  componentDidMount() {
    const name = this.props.match.params.id;
    fetch('https://api.thehealthodyssey.com/api/diseases/' + name)
        .then((res) => res.json())
        .then((data) => {
          const loc = {};
          const cause = {};
          const symp = {};
          for (const x in data.locations) {
            loc[x] = {name: data.locations[x], url: 'https://www.thehealthodyssey.com/Locations/' + data.locations[x]};
          }
          for (const x in data.causes) {
            cause[x] = {name: data.causes[x], url: 'https://www.thehealthodyssey.com/Causes/' + data.causes[x]};
          }
          for (const x in data.symptoms) {
            symp[x] = {name: data.symptoms[x]};
          }
          this.setState({
            locations: loc,
            disease: data,
            causes: cause,
            symptoms: symp,
          });
        });
  }

  // Adds all the locations with this disease to a class to add hyperlinks to each of them to their respective page

  addLocations() {
    const loc = [];
    let x = 0;
    for (x in this.state.locations) {
      loc.push(
          <URL {...this.state.locations[x]} />,
      );
    }
    loc.pop();
    loc.push(
        <URL_Last {...this.state.locations[x]} />,
    );
    return loc;
  }

  // Adds all the causes of a disease to a class to add hyperlinks to each of them to their respective page

  addCauses() {
    const cause = [];
    let x = 0;
    for (x in this.state.causes) {
      cause.push(
          <URL {...this.state.causes[x]} />,
      );
    }
    cause.pop();
    cause.push(
        <URL_Last {...this.state.causes[x]} />,
    );
    return cause;
  }

  addSymptoms() {
    const symp = [];
    let x = 0;
    for (x in this.state.symptoms) {
      symp.push(
          <Symptoms {...this.state.symptoms[x]} />,
      );
    }
    symp.pop();
    symp.push(
        <URL_Last {...this.state.symptoms[x]} />,
    );
    return symp;
  }

  render() {
    const locations = this.addLocations();
    const causes = this.addCauses();
    const symptoms = this.addSymptoms();

    return (
      <div className ="container">
        <h2>{this.state.disease.name}</h2>
        <table className="table table-striped" style={styles}>
          <thead>
            <tr>
              <th scope="col">Attribute</th>
              <th scope="col">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">Highest Concentrated Region</th>
              <td>{this.state.disease.highest_concentrated_region}</td>
            </tr>
            <tr>
              <th scope="row">Locations</th>
              <td>{locations}</td>
            </tr>
            <tr>
              <th scope="row">Causes</th>
              <td>{causes}</td>
            </tr>
            <tr>
              <th scope="row">Symptoms</th>
              <td>
                {symptoms}
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default DiseaseInstance;
