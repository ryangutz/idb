import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './stylesheet.css';
import Pagination from '../Pagination';
import Filter from './filterIndex';
import Highlighter from 'react-highlight-words';

class Diseases extends Component {
  constructor() {
    super();
    this.state = {
      currPage: 1,
      diseases: [],
      totalPages: 1,
      queryString: '&',
    };
    this.filtering = this.filtering.bind(this);
    this.searching = this.searching.bind(this);
  }

  filtering() {
    let queryString = '';
    const filterRegion = document.getElementById('highest_concentrated_region');
    const filterLoc = document.getElementById('search_location');
    const filterSymp = document.getElementById('search_symptom');
    const sort = document.getElementById('sort');
    const attr = document.getElementById('attribute');
    if (filterRegion.selectedIndex !== 0) {
      queryString += '&highest_concentrated_region=' + filterRegion.options[filterRegion.selectedIndex].value;
    }
    if (filterLoc.value) {
      queryString += '&locations=' + filterLoc.value;
    }
    if (filterSymp.value) {
      queryString += '&symptoms=' + filterSymp.value;
    }
    if (sort.value && attr.value) {
      queryString += '&sort=' + attr.value + sort.value;
    }
    console.log(queryString);
    fetch('https://api.thehealthodyssey.com/api/diseases?results_per_page=10' + queryString)
        .then((response) => response.json()).then((json) => {
          const diseases = json['objects'];
          const curr = json['page'];
          const total = json['total_pages'];
          this.setState({diseases: diseases, currPage: curr, totalPages: total, queryString: queryString});
        });
  }

  searching() {
    let queryString = '';
    const searchDis = document.getElementById('search_all');
    if (searchDis.value) {
      queryString += searchDis.value;
    }
    fetch('https://api.thehealthodyssey.com/api/diseases?search=' + queryString)
        .then((response) => response.json()).then((json) => {
          const diseases = json['objects'];
          const curr = json['page'];
          const total = json['total_pages'];
          this.setState({diseases: diseases, currPage: curr, totalPages: total, queryString: queryString});
        });
  }

  componentDidMount() {
    const querystring = this.props.location.search;
    let page = 1;
    let qs = '';
    if (querystring !== '') {
      const parsedQuerystring = querystring.substring(1);
      const queries = parsedQuerystring.split('&');
      queries.forEach((query) => {
        const keyValue = query.split('=');
        if (keyValue.length > 1 && keyValue[0] === 'page') {
          page = keyValue[1];
        } else if (keyValue.length > 1) {
          qs += '&'+query;
        }
      });
    }

    const url = 'https://api.thehealthodyssey.com/api/diseases?results_per_page=10&page=' + page + qs;
    fetch(url)
        .then((response) => response.json()).then((json) => {
          const diseases = json['objects'];
          const curpage = json['page'];
          const lastpage = json['total_pages'];
          this.setState({diseases: diseases, currPage: curpage, totalPages: lastpage, queryString: qs});
        });
  }


  render() {
    const renderedDiseases = this.state.diseases.map((d) => {
      return (
        <tr>
          <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.queryString]} autoEscape={true} textToHighlight={d.name}/></td>
          <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.queryString]} autoEscape={true} textToHighlight={d.highest_concentrated_region}/></td>
          <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.queryString]} autoEscape={true} textToHighlight={d.locations.join(', ')}/></td>
          <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.queryString]} autoEscape={true} textToHighlight={d.causes.join(', ')}/></td>
          <td><Highlighter highlightClassName="bg-warning" searchWords={[this.state.queryString]} autoEscape={true} textToHighlight={d.symptoms.join(', ')}/></td>
          <Link
            to={{
              pathname: `/Diseases/${d.name}`,
            }}
          >See More</Link>
        </tr>
      );
    });

    return (
      <div className = "container">
        <div className="form-inline col-lg-12 search">
          <input className="form-control col-lg-10" type="text"name="search_all"id="search_all"></input>
          <button className="btn btn-secondary col-lg-2" onClick={this.searching} type="submit">Search Diseases</button>
        </div>
        <Filter />
        <div className="text-center">
          <button className="btn btn-secondary" onClick={this.filtering}>Apply Filters</button>
        </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Highest-Concentrated-Region</th>
              <th scope="col">Locations</th>
              <th scope="col">Causes</th>
              <th scope="col">Symptoms</th>
            </tr>
          </thead>
          <tbody>
            {renderedDiseases}
          </tbody>
        </table>
        <Pagination
          url="/Diseases"
          query = {this.state.queryString}
          current={this.state.currPage}
          lastPage={this.state.totalPages}
        />
      </div>
    );
  }
}

export default Diseases;
