import React, { Component } from 'react';
import * as d3 from "d3";


class Chart extends Component{
    constructor(props) {
        super(props);
        this.state = {
            data : [],
            count: []
          };
    }

   

    async componentDidMount() {
        let data = [];
        let count = [];
      await fetch('https://api.thehealthodyssey.com/api/diseases')
        .then(response => response.json()).then(json => {
            json.objects.forEach(element => {
                let flag = 0;
              if(element.highest_concentrated_region === "n")
                  element.highest_concentrated_region = "Undefined";
              for(var x =0; x < data.length; x++){
                  if(data[x] === element.highest_concentrated_region)
                  {
                      count[x]++;
                      flag = 1;
                      break;
                  }
                  else
                      flag = 0;
              }
              if(!flag){  
                  data.push(element.highest_concentrated_region);
                  count.push(1);
              }
            });
        })

        this.setState({ data : data, count : count});
        this.createBarChart();
    }

  
    createBarChart(){
        var margin = {top: 20, right: 50, bottom: 30, left: 150},
        width = 1200 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;

        var y = d3.scaleBand()
                  .domain(this.state.data)
                .range([height,0])
                .padding(0.35);

        var x = d3.scaleLinear()
                  .domain([0, d3.max(this.state.count)]).nice()
                  .range([0, width]);

        var svg = d3.select("#chart").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom + 50)
          .append("g")
            .attr("transform", 
                  "translate(" + margin.left + "," + margin.top + ")");

          svg.selectAll(".bar")
              .data(this.state.count)
              .enter().append("rect")
              .style('fill', '#CD96CD')
              .attr("class", "bar")
              .attr("width", d => x(d))
              .attr("y", (d, i) => y(this.state.data[i]))
              .attr("height", y.bandwidth());

          // add the x Axis
          svg.append("g")
              .attr("transform", "translate(0," + height + ")")
              .call(d3.axisBottom(x));

          // add the y Axis
          svg.append("g")
              .call(d3.axisLeft(y));

           svg.append("g")
            .attr("transform", `translate(0,${height - margin.bottom})`)
            .append('text')
            .attr('x', width/2)
            .attr('y', 80)
            .attr('fill', '#000')
            .style('font-size', '20px')
            .style('text-anchor', 'middle')
            .text('Total of Diseases Highest Concentrated Region');
;

        svg.append("g")
            .attr("transform", `translate(${margin.left},0)`)
            .append('text')
            .attr('x', 0)
            .attr('y', 0)
            .attr('transform', 'translate(-'+(margin.left + 90)+`, 240) rotate(-90)`)
            .attr('fill', '#000')
            .style('font-size', '20px')
            .style('text-anchor', 'middle')
            .text('Region');




    }
    render() {

        return (
            <div align="center">
                <h1>Totaling the Diseases Highest Concentrated Regions</h1>
                <p> This visualization depicts the number of diseases that consider
                    a region to have the highest concentration of that disease.</p>
             
                    <svg id='chart' 
                         width={1400} height={700}>
                    </svg>
       
            </div>
        );
    }
  }




export default Chart;
