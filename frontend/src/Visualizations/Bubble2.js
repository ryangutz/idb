import React, {Component} from 'react';
import BChart from '@weknow/react-bubble-chart-d3';


class Bubble2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }


  componentDidMount() {
      fetch('https://api.scholarsearch.net/search/cities')
          .then((response) => response.json()).then((json) => {
            const data = json['data'];
            let temp = [];
            let flag = 0;
            data.forEach(element => {
                for(var x = 0; x < temp.length; x++){
                    if(temp[x].label === element.state)
                    {
                        temp[x].value++;
                        flag = 1;
                        break;
                    }
                    else
                        flag = 0;
                }

                if(!flag){
                    temp.push({
                        label : element.state,
                        value : 1
                    })
                }
                else
                    flag = 0;

            });
            this.setState({data: temp});
          });

  }

  

  render() {

    return (
        <div align = "center">
        <h1>Number of Colleges per State</h1>
        <p>A visualization of the number of colleges that exist in each state<br />
          based on our developer's API displayed in a bubble chart format.
        </p>
    <BChart
    graph={{
        zoom: .7,
        offsetX: .14,
        offsetY: .03,
    }}
    width={1400}
    height={1100}
    padding={0} // optional value, number that set the padding between bubbles
    showLegend={false} // optional value, pass false to disable the legend.
    valueFont={{
        family: 'Arial',
        size: 18,
        color: '#000',
        weight: 'bold',
    }}
    labelFont={{
        family: 'Arial',
        size: 10,
        color: '#000',
        weight: 'bold',
    }}
    
    data = {this.state.data}
    />
    </div>
    );
  }
}


export default Bubble2;