import React, { Component } from "react";

import BarChart from "./BarChart";
import BarChart2 from "./BarChart2";
import Bubble from "./Bubble";
import TuitionVisual from "./tuitionChart";
import Chart1 from "./devVisual1";
import Bubble2 from "./Bubble2"

import './Visualizations.css';
import 'bootstrap/dist/css/bootstrap.min.css';


class Visualizations extends React.Component{
 
    render(){
  
      return (
     <div>
       <div className="margin">
       <div className="bottomMargin">
      	<h2>Visualizations</h2>
        </div>
          <BarChart size={[window.screen.width / 5 * 4, 500]}/>
          <Bubble />
          <BarChart2 size={[window.screen.width / 5 * 4, 500]}/>
	  


      </div>
      <div>
      <div className="bottomMargin">
      	<h2>Developer Visualizations</h2>
          <h3>Scholar Search</h3></div>
          <Chart1 />
          <Bubble2 />
          <TuitionVisual width={window.screen.width / 5 * 4} height="500"/>
        </div></div>
    );
  }
}

export default Visualizations;
