import React from 'react'
import * as d3 from 'd3';
import './Visualizations.css'


class devVisual1 extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        data: [],
        check: [],
        underFive: 0,
        fiveToTen: 0,
        tenToFifteen: 0,
        fifteenToTwenty: 0,
        overTwenty: 0,
        loaded: false
      };
    }

  async componentDidMount() {
    await fetch('https://api.scholarsearch.net/search/colleges')
        .then(response => response.json()).then(json => {
            json.data.forEach(element => {
               if (element.in_state_tuition) {
                this.state.check.push(element.in_state_tuition);
            }
                if (element.in_state_tuition < 5000.0){
            this.state.underFive += 1;
          }
          else if (element.in_state_tuition < 10000.0){
            this.state.fiveToTen += 1
          }
          else if (element.in_state_tuition < 15000.0){
            this.state.tenToFifteen += 1
          }
          else if (element.in_state_tuition < 20000.0){
            this.state.fifteenToTwenty += 1
          }
          else{
            this.state.overTwenty += 1
          }
          
          

        });
        this.state.data.push({
          name: '<5',
          value: this.state.underFive
        });
        this.state.data.push({
          name: '5-10',
          value: this.state.fiveToTen
        });
        this.state.data.push({
          name: '10-15',
          value: this.state.tenToFifteen
        });
        this.state.data.push({
          name: '15-20',
          value: this.state.fifteenToTwenty
        });
        this.state.data.push({
          name: '>20',
          value: this.state.overTwenty
        });
        
        

      }).then(() => {
        this.setState({loaded: true});
        this.createPieChart();
      });
      console.log(this.state.data);
      console.log(this.state.underTen);
      console.log(this.state.fortyToFifty);
      console.log(this.state.overFifty);


  }

  createPieChart() {

    let width = 700;
    let height = 700;
    let margin = 30;

    let radius = Math.min(width, height) / 3 - margin;

    const svg = d3.select(this.refs.canvas)
      .append("svg")
      .attr('preserveAspectRatio', 'xMinYMin meet')
      .attr('viewBox', '0 0 700 700')
      .classed('svg-content', true)
      .append("g")
      .attr("transform", "translate(" + width / 2 + "," + height / 3 + ")");

    let data = {};
    this.state.data.forEach((stat) => {
      
      data[stat.name] = stat.value;
    });

    let color = d3.scaleOrdinal()
      .domain(data)
      .range(d3.schemeSet1);

      let pie = d3.pie()
      .value(function (d) {
          return d.value;
      });

    let data_ready = pie(d3.entries(data));

    let arcGenerator = d3.arc()
      .innerRadius(0)
      .outerRadius(radius);

    svg
      .selectAll('mySlices')
      .data(data_ready)
      .enter()
      .append('path')
      .attr('d', arcGenerator)
      .attr('fill', function (d) {
          return (color(d.data.key))
      })
      .attr("stroke", "white")
      .style("stroke-width", "1px")
      .style("opacity", 0.7);

    svg
      .selectAll('mySlices')
      .data(data_ready)
      .enter()
      .append('text')
      .text(function (d) {
          return d.data.key
      })
      .attr("transform", function (d) {
          return "translate(" + arcGenerator.centroid(d) + ")";
      })
      .style("text-anchor", "middle")
      .style("font-size", 12)
  }




render() {
  
    return (
      <React.Fragment>
        <h3 className="text-center"><span>In-State-Tuition Chart</span></h3>
        <h1 className="text-center"><span>(in thousands)</span></h1>

        <div className="row">
          <div className="col-md-2">
            <div className="sideMargin">
              <p><strong>Number of colleges and categorized by in-state-tuition cost(in thousands)</strong></p>
              <p>Under $5,000: {this.state.underFive}</p>
              <p>Between $5,000 and $10,000: {this.state.fiveToTen}</p>
              <p>Between $10,000 and $15,000: {this.state.tenToFifteen}</p>
              <p>Between $15,000 and $20,000: {this.state.fifteenToTwenty}</p>
              <p>Over $20,000: {this.state.overTwenty}</p>
              

              
            </div>
          </div>
          <div className="col-md-9">
              <div ref="canvas" className="col-md-12"></div>
          
         
          </div>
        </div>
      </React.Fragment>
    );
  }

}

export default devVisual1
