import React, { Component } from 'react';
import BChart from '@weknow/react-bubble-chart-d3';

class Bubble extends Component{
    state = {
        data: {}
    }
    
    componentDidMount() {
        var colors = [{color:"red", loc:"Africa"},{color:"#00ccff", loc:"Americas"}, 
    {color:"yellow", loc:"Eastern Mediterranean"}, {color:"green", loc:"Europe"},
    {color:"purple", loc:"South-East Asia"}, {color:"orange", loc:"Western Pacific"} ];
        fetch('https://api.thehealthodyssey.com/api/locations')
        .then(response => response.json()).then(json => {
            let data = [];
            json.objects.forEach(element => {
                let color = ""
                for(let x=0;x<6;x++){
                    if(colors[x].loc === element.region) {
                        color = colors[x].color;
                        break;
                    }
                    else
                        color = "pink";
                    }
                data.push({
                    label : element.name,
                    value : element.number_of_diseases,
                    color : color
                })
            });
            this.setState({ data : data});
        })
        
    }
    render() {
        return(
            <div align = "center">
                <h1>Number of Diseases in Each Country</h1>
                <p>A visualization colored by region that contains the number of diseases it has
                present in our database.
                </p>
            <BChart
            graph={{
                zoom: .7,
                offsetX: .14,
                offsetY: .03,
            }}
            width={1200}
            height={1000}
            showLegend={false} //Disable legend, not useful in our setup
            valueFont={{
                family: 'Arial',
                size: 18,
                color: '#000',
                weight: 'bold',
            }}
            labelFont={{
                family: 'Arial',
                size: 10,
                color: '#000',
                weight: 'bold',
            }}

            data = {this.state.data}
        />
        </div>
        )

    }
}


export default Bubble;