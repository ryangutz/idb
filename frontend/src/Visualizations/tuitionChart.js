import React from 'react';
import * as d3 from 'd3';

const QUERY = "https://api.scholarsearch.net/search/colleges"

class TuitionVisual extends React.Component{
constructor(props) {
    super(props);
    this.state = {
      width: props.width || null,
      height: props.height || null
    };
    this.updateVisual = this.updateVisual.bind(this);
  }

  componentDidMount(){
    this.getData();
    this.updateVisual();
  }

  componentDidUpdate(){
    this.updateVisual();
  }

  getQData(){
    return fetch(QUERY)
      .then((resp) => {
        return resp.json();
      });
  }

  getData(){
    return Promise.all([this.getQData()]);
  }

  updateVisual(){
    const node = this.node;

    this.getData()
      .then((data) => {
        var dataPoints = {};
	console.log(data);
	console.log(data[0].data);
        (data[0].data).forEach((item) => {
          var key = "" + item.name; 
          if(! (key in dataPoints)){            
            dataPoints[key] = {
              name: key,
	      admission_rate: item.admission_rate,
	      in_state_tuition: item.in_state_tuition,
	      out_of_state_tuition: item.out_of_state_tuition,
            };
          }
        })
    
    var keys = [];
    var admission = [];
    var in_state_tuition = [];
    var out_of_state_tuition = [];
    for(var key in dataPoints){
      if(dataPoints[key].admission_rate != null){
        keys.push(dataPoints[key].name);
        admission.push(dataPoints[key].admission);
        in_state_tuition.push(dataPoints[key].in_state_tuition);
        out_of_state_tuition.push(dataPoints[key].out_of_state_tuition);
      }
    }
    
    const margin = {
      top: 30,
      bottom: 30,
      left: 100,
      right: 50,
    };
    var paddingLeft = 100;
    var paddingBottom = 30;
    var paddingTop = 30;
    var paddingRight = 50;

    const tuitionMax = d3.max(out_of_state_tuition);

    const xScale = d3.scaleLinear()
      .domain([0, 1.0])
      .range([paddingLeft, this.state.width - paddingRight]);

    console.log(0.5);
    console.log(xScale(0.5));
	      

    const yScale = d3.scaleLinear()
      .domain([0, tuitionMax])
      .range([this.state.height - paddingBottom, paddingTop]);

    const xAxis = d3.axisBottom(xScale);

    const yAxis = d3.axisLeft(yScale);

    //Add y axis to image
    d3.select(node)
      .append('g')
      .attr('id', 'yAxis')
      .attr('transform', 'translate(' + paddingLeft + ',0)')
      .call(yAxis);

    //Add x axis to image
    d3.select(node)
      .append('g')
      .attr('id', 'xAxis')
      .attr('transform', 'translate(0, ' + (this.state.height - paddingBottom) + ')')
      .call(xAxis);
    
    d3.select(node)
      .append('text')
      .attr('x', xScale(1.0 / 2))
      .attr('y', this.state.height - 60)
      .attr('text-anchor', 'middle')
      .text('Acceptance Rate');

    d3.select(node)
      .append('text')
      .attr('x', 30)
      .attr('y', yScale(tuitionMax / 2))
      .attr('text-anchor', 'middle')
      .attr('transform', 'rotate(270, 30,' + yScale(tuitionMax/2) + ')')
      .text('Tuition');

    //Link the data to g tags and add a child rect
    d3.select(node)
      .selectAll('#point')
      .data(keys)
      .enter()
      .append('g')
      .attr('id', 'point')
      .append('circle')
      .style('fill', '#007bff')
      .attr('cx', (d) => xScale(dataPoints[d].admission_rate))
      .attr('cy', (d) => yScale(dataPoints[d].out_of_state_tuition))
      .attr('r', 5);

    d3.select(node)
      .selectAll('#point')
      .data(keys)
      .enter()
      .append('g')
      .attr('id', 'point')
      .append('circle')
      .style('fill', '#ff7b00')
      .attr('cx', (d) => xScale(dataPoints[d].admission_rate))
      .attr('cy', (d) => yScale(dataPoints[d].in_state_tuition))
      .attr('r', 5);
    });
  }

  render() {
    return(<div align="center"> <h1>Admission rate vs. out of state tuition</h1>
	    <p> This visualization shows the relationship between a schools acceptance rate and its out of state tuition costs. </p>
      <svg ref={node => this.node = node}
      width={this.state.width} height={this.state.height}>
      </svg></div>
    );
  }

}

export default TuitionVisual;

