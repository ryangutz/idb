import React, {Component} from 'react';
import {scaleLinear, scaleBand} from 'd3-scale';
import {max} from 'd3-array';
import {select} from 'd3-selection';
import {axisBottom, axisLeft} from 'd3-axis';
import {format} from 'd3-format';
import * as d3 from 'd3';
import axios from 'axios';

const regions = ['Africa', 'Americas', 'Eastern Mediterranean', 'Europe', 'South-East Asia', 'Western Pacific'];

class Chart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      avg_hospitals: [0, 0, 0, 0, 0, 0],
    };
  }


  componentDidMount() {
    for (let i = 0; i < regions.length; i++) {
      fetch('https://api.thehealthodyssey.com/api/locations?region=' + regions[i])
          .then((response) => response.json()).then((json) => {
            const data = json['objects'];
            this.getName(data, i);
            this.setState({data: data});
          });
    }
    if (!(this.state.avg_hospitals).includes(0)) {
      this.createBarChart();
    }
  }

  getName(data, num) {
    for (let i = 0; i < data.length; i++) {
      this.state.avg_hospitals[num] = this.state.avg_hospitals[num] + data[i].hospitals_per_100k;
    }
    this.state.avg_hospitals[num] = this.state.avg_hospitals[num]/data.length;
  }


  componentDidUpdate() {
    if (!(this.state.avg_hospitals).includes(0)) {
      this.createBarChart();
    }
  }
  createBarChart() {
    const margin = {
      top: 30,
      bottom: 30,
      left: 100,
      right: 50,
    };

    const width = 1400;
    const height = 500;
    console.log(this.state.avg_hospitals);
    const x = d3.scaleBand()
        .domain(regions)
        .range([margin.left, width - margin.right])
        .padding(0.1);

    const y = d3.scaleLinear()
        .domain([0, d3.max(this.state.avg_hospitals)]).nice()
        .range([height - margin.bottom, margin.top]);

    const xAxis = (g) => g
        .attr('transform', `translate(0,${height - margin.bottom})`)
        .call(d3.axisBottom(x)
            .tickSizeOuter(0));

    const yAxis = (g) => g
        .attr('transform', `translate(${margin.left},0)`)
        .call(d3.axisLeft(y))
        .call((g) => g.select('.domain').remove());


    const svg = d3.select('#body3');

    svg.append('g')
        .style('fill', '#69b3a2')
        .selectAll('rect').data(this.state.avg_hospitals).enter().append('rect')
        .attr('x', (d, i) => x(regions[i]))
        .attr('y', (d) => y(d))
        .attr('height', (d) => y(0) - y(d))
        .attr('width', x.bandwidth());

    svg.append('g')
        .call(xAxis)
        .append('text')
        .attr('x', this.props.size[0]/2)
        .attr('y', 45)
        .attr('fill', '#000')
        .style('font-size', '20px')
        .style('text-anchor', 'middle')
        .text('Region');
    ;

    svg.append('g')
        .call(yAxis)
        .append('text')
        .attr('x', 0)
        .attr('y', 0)
        .attr('transform', 'translate(-'+(margin.left - 40)+`, ${this.props.size[1]/2}) rotate(-90)`)
        .attr('fill', '#000')
        .style('font-size', '20px')
        .style('text-anchor', 'middle')
        .text('Hospitals per 100,000 people');
  }
  render() {
    // if (!this.state.done) {
    //     return <h1>Loading</h1>
    // }
    return (
      <div align="center">
        <h1>Average Number of Hospitals per 100,000 People by Region</h1>
        <p> This visualization depicts the average number of hospitals that are present per every 100,000 people in each of the 6 regions we
                have in our database.</p>

        <svg id='body3'
          width={1400} height={650}>
        </svg>

      </div>
    );
  }
}


export default Chart;
