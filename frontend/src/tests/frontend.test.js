import waitUntil from "async-wait-until";
import React from 'react';
import { assert } from "chai";
import { expect } from "chai";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import fetch from "isomorphic-fetch";
import Diseases from '../Diseases';
import Locations from '../Locations';
import Causes from '../Causes';

import DiseasesInstance from '../Diseases/DiseaseInstance';
import LocationInstance from '../Locations/LocationInstance';
import CauseInstance from '../Causes/CausesInstance/CausesInstance'; 

configure({ adapter: new Adapter() });

describe("Diseases test", function() {
  let querystring = {
    search: "?page=1"
  };

  it("Diseases doesn't have any data at first render", function() {
    const root = shallow(<Diseases location={querystring} />);
    assert.equal(root.state("diseases").length, 0);
  });


});

describe("Locations test", function() {
  let querystring = {
    search: "?page=1"
  };

  it("Locations doesn't have any data at first render", function() {
    const root = shallow(<Locations location={querystring} />);
    assert.equal(root.state("locations").length, 0);
  });
  it("Locations load correctly from api", function(done) {
    const root = shallow(<Locations location={querystring} />);
    waitUntil(() => root.state("locations").length > 0).then(() => {
      assert.equal(root.state("locations").length, 10);
      done();
    });
  });


});

describe("Causes test", function() {
  let querystring = {
    search: "?page=1"
  };

  it("Causes doesn't have any data at first render", function() {
    const root = shallow(<Causes location={querystring} />);
    assert.equal(root.state("causes").length, 0);
  });
  it("Causes load correctly from api", function(done) {
    const root = shallow(<Causes location={querystring} />);
    waitUntil(() => root.state("causes").length > 0).then(() => {
      assert.equal(root.state("causes").length, 10);
      done();
    });
  });


});

describe("Disease Instance test", function() {
  const match = {
    params: {
      id: "Cholera"
    }
  };
  it("Disease Instance page doesn't have any data at first render", function() {
    const root = shallow(<DiseasesInstance match={match} />);
    assert.equal(root.state("disease").id, null);
  });

});



describe("Location Instance test", function() {
  const match = {
    params: {
      id: "Burundi"
    }
  };
  it("Location Instance page doesn't have any data at first render", function() {
    const root = shallow(<LocationInstance match={match} />);
    assert.equal(root.state("location").id, null);
  });

  it("Location Instance page loads correctly from api", function(done) {
    const root = shallow(<LocationInstance match={match} />);
    waitUntil(() => root.state("location").id != null).then(() => {
      assert.equal(root.state("location").id, 1);
      assert.equal(root.state("location").name, "Burundi");
      assert.equal(root.state("location").region, "Africa");
      done();
    });
  });
});

describe("Cause Instance test", function() {
  const match = {
    params: {
      id: "Vibrio cholerae"
    }
  };
  it("Cause Instance page doesn't have any data at first render", function() {
    const root = shallow(<CauseInstance match={match} />);
    assert.equal(root.state("cause").id, null);
  });

  it("Cause Instance page loads correctly from api", function(done) {
    const root = shallow(<CauseInstance match={match} />);
    waitUntil(() => root.state("cause").id != null).then(() => {
      assert.equal(root.state("cause").id, 1);
      assert.equal(root.state("cause").name, "Vibrio cholerae");
      assert.equal(root.state("cause").reaction_time, "+12 to +72");
      done();
    });
  });
});

describe("Searching test", function() {
  let querystring = {
    pathname: "/locations?search",
    search: "=Europe"
  };

  it("Searched Locations don't have any data at first render", function() {
    const root = shallow(<Locations location={querystring} />);
    assert.equal(root.state("locations").length, 0);
  });

  it("Searched Locations load correctly from api", function(done) {
    const root = shallow(<Locations location={querystring} />);
    waitUntil(() => root.state("locations").length > 0).then(() => {
      assert.equal(root.state("locations").length, 10);
      done();
    });
  });
});

describe("Sorting test", function() {
  let querystring = {
    pathname: "/locations/sort",
    search: "?sort=name:desc"
  };

  it("Sorted Locations don't have any data at first render", function() {
    const root = shallow(<Locations location={querystring} />);
    assert.equal(root.state("locations").length, 0);
  });

  it("Sorted Locations load correctly from api", function(done) {
    const root = shallow(<Locations location={querystring} />);
    waitUntil(() => root.state("locations").length > 0).then(() => {
      assert.equal(root.state("locations").length, 10);
      assert.equal(root.state("locations")[0].id, "10");
      done();
    });
  });
});

describe("Filtering test", function() {
  let querystring = {
    pathname: "/locations?search",
    search: "=Western Pacific"
  };

  it("Filtered Locations don't have any data at first render", function() {
    const root = shallow(<Locations location={querystring} />);
    assert.equal(root.state("locations").length, 0);
  });

  it("Filtered Locations load correctly from api", function(done) {
    const root = shallow(<Locations location={querystring} />);
    waitUntil(() => root.state("locations").length > 0).then(() => {
      assert.equal(root.state("locations").length, 10);
      done();
    });
  });
});







