import json
import requests
URL_Locations = 'http://0.0.0.0:5000/api/locations'
URL_Diseases = 'http://0.0.0.0:5000/api/diseases'
URL_Causes = 'http://0.0.0.0:5000/api/causes'
URL_Transmissions = 'http://0.0.0.0:5000/api/transmissions'
URL_Symptoms = 'http://0.0.0.0:5000/api/symptoms'
URL_Treatments = 'http://0.0.0.0:5000/api/treatments'
URL_DisTrans = 'http://0.0.0.0:5000/api/diseases_transmissions'
URL_DisSymp = 'http://0.0.0.0:5000/api/diseases_symptoms'
URL_DisTreat = 'http://0.0.0.0:5000/api/diseases_treatments'
URL_DisLoc = 'http://0.0.0.0:5000/api/diseases_locations'
URL_CauseTrans = 'http://0.0.0.0:5000/api/causes_transmissions'
URL_CauseTreat = 'http://0.0.0.0:5000/api/causes_treatments'
URL_LocCauses = 'http://0.0.0.0:5000/api/locations_causes'
URL_CausesDis = 'http://0.0.0.0:5000/api/causes_diseases'

def load_information():
    # ---------
    # Locations
    # ---------

    with open('locations.json', 'r') as f:
        locations = json.load(f)

    locations_data = locations['data']
    # for location in locations_data:
    #     j = {
    #         "name": location['name'],
    #         "region": location['region'],
    #         "avg_age": location['avg_age'],
    #         "preventable_deaths": location['preventable_deaths'],
    #         "mortality_rate": location['mortality_rate'],
    #         "hospitals_per_100k": location['hospitals_per_100k'],
    #         "number_of_diseases": location['number_of_diseases'],
    #         "population": location['population']
    #     }
    #     file = json.dumps(j)
    #     r = requests.post(url = URL_Locations, data = file, headers={'Content-Type': 'application/json'})
    #     print(r)

    # # --------
    # # Diseases
    # # --------

    with open('diseases.json', 'r') as f:
        diseases = json.load(f)

    diseases_data = diseases['data']
    # for disease in diseases_data:
    #     j = {
    #         "name": disease['name'],
    #         "image": disease['image'],
    #         "highest_concentrated_region": disease['highest_concentrated_region']
    #     }
    #     disease_file = json.dumps(j)
    #     r = requests.post(url = URL_Diseases, data = disease_file, headers={'Content-Type': 'application/json'})
    #     print(r)

    #     r = requests.get(url = URL_Diseases + '/' + disease['name'], headers={'Content-Type': 'application/json'})
    #     print(r)
    #     disease_id = r.json()['id']

    #     for transmission in disease['spread_type']:
    #         if transmission:
    #             trans = {
    #                 "name": transmission
    #             }
    #             trans_file = json.dumps(trans)
    #             r = requests.post(url = URL_Transmissions, data = trans_file, headers={'Content-Type': 'application/json'})
    #             print(r)

    #             r = requests.get(url = URL_Transmissions + '/' + transmission, headers={'Content-Type': 'application/json'})
    #             transmission = r.json()
                
    #             link = {
    #                 "disease_id": disease_id,
    #                 "trans_id": transmission['id'],
    #                 "disease_name": disease['name'],
    #                 "trans_name": transmission['name']
    #             }
    #             link_file = json.dumps(link)
    #             r = requests.post(url = URL_DisTrans, data = link_file, headers={'Content-Type': 'application/json'})
    #             print(r)
    #     for symptom in disease['symptoms']:
    #         if symptom:
    #             symp = {
    #                 "name": symptom
    #             }
    #             symp_file = json.dumps(symp)
    #             r = requests.post(url = URL_Symptoms, data = symp_file, headers={'Content-Type': 'application/json'})
    #             print(r)

    #             r = requests.get(url = URL_Symptoms + '/' + symptom, headers={'Content-Type': 'application/json'})
    #             symptom = r.json()
                
    #             link = {
    #                 "disease_id": disease_id,
    #                 "symptom_id": symptom['id'],
    #                 "disease_name": disease['name'],
    #                 "symptom_name": symptom['name']
    #             }
    #             link_file = json.dumps(link)
    #             r = requests.post(url = URL_DisSymp, data = link_file, headers={'Content-Type': 'application/json'})
    #             print(r)
    #     for treatment in disease['treatment']:
    #         if treatment:
    #             treat = {
    #                 "name": treatment
    #             }
    #             treat_file = json.dumps(treat)
    #             r = requests.post(url = URL_Treatments, data = treat_file, headers={'Content-Type': 'application/json'})
    #             print(r)

    #             r = requests.get(url = URL_Treatments + '/' + treatment, headers={'Content-Type': 'application/json'})
    #             treatment = r.json()
                
    #             link = {
    #                 "disease_id": disease_id,
    #                 "treatment_id": treatment['id'],
    #                 "disease_name": disease['name'],
    #                 "treatment_name": treatment['name']
    #             }
    #             link_file = json.dumps(link)
    #             r = requests.post(url = URL_DisTreat, data = link_file, headers={'Content-Type': 'application/json'})
    #             print(r)
    #     for location in disease['locations']:
    #         if location:
    #             r = requests.get(url = URL_Locations + '/' + location, headers={'Content-Type': 'application/json'})
    #             location = r.json()
                
    #             if r.ok:
    #                 link = {
    #                     "disease_id": disease_id,
    #                     "location_id": location['id'],
    #                     "disease_name": disease['name'],
    #                     "location_name": location['name']
    #                 }
    #                 link_file = json.dumps(link)
    #                 r = requests.post(url = URL_DisLoc, data = link_file, headers={'Content-Type': 'application/json'})
    #                 print(r)

    # ------
    # Causes
    # ------

    """
    NOT TESTED
    """

    with open('causes.json', 'r') as f:
        causes = json.load(f)

    causes_data = causes['data']
    for cause in causes_data:
        j = {
            "name": cause['name'],
            "reaction_time": cause['reaction_time'],
            "image": cause['image']
        }
        cause_file = json.dumps(j)
        r = requests.post(url = URL_Causes, data = cause_file, headers={'Content-Type': 'application/json'})
        print(r)

        r = requests.get(url = URL_Causes + '/' + cause['name'], headers={'Content-Type': 'application/json'})
        print(r)
        cause_id = r.json()['id']

        for transmission in cause['transmission']:
            if transmission:
                trans = {
                    "name": transmission
                }
                trans_file = json.dumps(trans)
                r = requests.post(url = URL_Transmissions, data = trans_file, headers={'Content-Type': 'application/json'})
                print(r)

                r = requests.get(url = URL_Transmissions + '/' + transmission, headers={'Content-Type': 'application/json'})
                transmission = r.json()
                
                link = {
                    "cause_id": cause_id,
                    "trans_id": transmission['id'],
                    "cause_name": cause['name'],
                    "trans_name": transmission['name']
                }
                link_file = json.dumps(link)
                r = requests.post(url = URL_CauseTrans, data = link_file, headers={'Content-Type': 'application/json'})
                print(r)
        for treatment in cause['treatment']:
            if treatment:
                treat = {
                    "name": treatment
                }
                treat_file = json.dumps(treat)
                r = requests.post(url = URL_Treatments, data = treat_file, headers={'Content-Type': 'application/json'})
                print(r)

                r = requests.get(url = URL_Treatments + '/' + treatment, headers={'Content-Type': 'application/json'})
                treatment = r.json()
                
                link = {
                    "cause_id": cause_id,
                    "treatment_id": treatment['id'],
                    "cause_name": cause['name'],
                    "treatment_name": treatment['name']
                }
                link_file = json.dumps(link)
                r = requests.post(url = URL_CauseTreat, data = link_file, headers={'Content-Type': 'application/json'})
                print(r)
        for location in cause['locations']:
            if location:
                r = requests.get(url = URL_Locations + '/' + location, headers={'Content-Type': 'application/json'})
                location = r.json()
                
                if r.ok:
                    link = {
                        "cause_id": cause_id,
                        "location_id": location['id'],
                        "cause_name": cause['name'],
                        "location_name": location['name']
                    }
                    link_file = json.dumps(link)
                    r = requests.post(url = URL_LocCauses, data = link_file, headers={'Content-Type': 'application/json'})
                    print(r)
        for disease in cause['disease']:
            if disease:
                r = requests.get(url = URL_Diseases + '/' + disease, headers={'Content-Type': 'application/json'})
                location = r.json()
                
                if r.ok:
                    link = {
                        "cause_id": cause_id,
                        "disease_id": disease['id'],
                        "cause_name": cause['name'],
                        "disease_name": disease['name']
                    }
                    link_file = json.dumps(link)
                    r = requests.post(url = URL_CausesDis, data = link_file, headers={'Content-Type': 'application/json'})
                    print(r)


if __name__ == "__main__":
	load_information()