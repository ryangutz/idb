import json
import requests
URL = 'http://0.0.0.0:5000/api/locations'

def clean_locations():
	r = requests.get(url = URL, headers={'Content-Type': 'application/json'})

	data = r.json()
	for i in range(1, data['total_pages'] + 1):
		r = requests.get(url = URL, headers={'Content-Type': 'application/json'})
		locations = r.json()
		for location in locations['objects']:
			url = URL
			url += '/'
			url += str(location['id'])
			print(url)
			r = requests.delete(url = url)

if __name__ == "__main__":
	clean_locations()