import json
import requests
URL = 'http://0.0.0.0:5000/api/causes'

def clean_causes():
	r = requests.get(url = URL, headers={'Content-Type': 'application/json'})

	data = r.json()
	for i in range(1, data['total_pages'] + 1):
		r = requests.get(url = URL, headers={'Content-Type': 'application/json'})
		causes = r.json()
		for cause in causes['objects']:
			url = URL
			url += '/'
			url += str(cause['id'])
			print(url)
			r = requests.delete(url = url)

if __name__ == "__main__":
	clean_causes()