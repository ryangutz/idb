import json
import requests
URL = 'http://0.0.0.0:5000/Locations'

def update_locations():
	with open('Scrapper/locations.json', 'r') as f:
		locations = json.load(f)

	data = locations['data']
	for location in data:
		j = {
		    "name": location['name'],
		    "region": location['region'],
		    "population": location['population'],
		    "diseases": location['diseases'],
		    "preventable_deaths": location['preventable_deaths'],
		    "cause_names": location['cause_names'],
		    "number_of_diseases": location['number_of_diseases'],
		    "mortality_rate": location['mortality_rate'],
		    "avg_age": location['avg_age'],
		    "hospitals_per_sqmi": location['hospitals_per_100k'],
		    "similar_countries": location['similar_countries']
		}
		file = json.dumps(j)
		r = requests.put(url = URL, data = file)
		print(r)

if __name__ == "__main__":
	update_locations()