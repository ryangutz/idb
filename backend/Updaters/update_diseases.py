import json
import requests
URL = 'http://0.0.0.0:5000/api/diseases'

def update_diseases():
	with open('/Scrapper/diseases.json', 'r') as f:
		diseases = json.load(f)

	data = diseases['data']
	for disease in data:
		j = {
			"origin": disease['origin'],
			"name": disease['name'],
			"locations": disease['locations'],
			"highest_concentrated_region": disease['highest_concentrated_region'],
			"symptoms": disease['symptoms'],
			"spread_type": disease['spread_type'],
			"causes": disease['causes'],
			"avg_treatment_cost": disease['avg_treatment_cost'],
			"prevention": disease['prevention'],
			"death_rate": disease['death_rate'],
		}
		file = json.dumps(j)
		r = requests.put(url = URL, data = file, headers={'Content-Type': 'application/json'})

if __name__ == "__main__":
	update_diseases()