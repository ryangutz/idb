import json
import requests
URL = 'http://0.0.0.0:5000/api/causes'

def update_causes():
	with open('/Scrapper/causes.json', 'r') as f:
		causes = json.load(f)

	data = causes['data']
	for cause in data:
		j = {
		    "name": cause['name'],
			"diseases": cause['diseases'],
			"prevention": causea['prevention'],
			"locations": cause['locations'],
			"reaction_time": cause['reaction_time'],
			"importance": cause['importance'],
			"treatment": cause['treatment'],
			"cheapest_treatment": cause['cheapest_treatment'],
			"most_expensive_treatment": cause['most_expensive_treatment'],
			"related_causes": cause['related_causes']
		}
		file = json.dumps(j)
		r = requests.put(url = URL, data = file, headers={'Content-Type': 'application/json'})

if __name__ == "__main__":
	update_causes()