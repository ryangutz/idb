import requests
import json
from pprint import pprint

who_url="https://ghoapi.azureedge.net/api/"
disease_pages = {
        "Cholera":"WHS3_40",
        "Diphtheria":"WHS3_41",
        "Japanese encephalitis":"WHS3_42",
        "Pertussis":"WHS3_43",
        "Leprosy":"WHS3_45",
        "Tetanus":"WHS3_46",
        "Meningitis":"WHS3_47",
        "Malaria":"WHS3_48",
        "Poliomyelitis":"WHS3_49",
        "Yellow fever":"WHS3_50",
        "H5N1 influenza":"WHS3_51",
        "Plague":"WHS3_52",
        "Tuberculosis":"WHS3_522",
        "Mumps":"WHS3_53",
        "Rubella":"WHS3_57",
        "Measles":"WHS3_62"
        }


#id
#cause names
#disease names
#prevention
#location names
#reaction time
#importance
#treatment
#cheapest treatment cost
#most expensive treatment cost
#related causes
#https://ghoapi.azureedge.net/api/WSH_WATER_SAFELY_MANAGED
#cholera


def main():
    causes=[]
    i=0
    for disease in disease_pages:
        cause={}
        cause["id"]=i
        cause["disease"] = disease
        cause["prevention"] = "prevention"
        cause["locations"] = "country 1, country 2, country 3"
        cause["reaction_time"] = "0d"
        cause["importance"] = "??"
        cause["treatment"] = "not treatable"
        cause["cheapest_treatment"] = "$1"
        cause["most_expensive_treatment"] = "$1000000000"
        cause["related_causes"] = "cause 1, cause 2"
        causes.append(cause)
    
    json_causes = {"data": causes}
    with open("causes.json", "w") as f:
        json.dump(json_causes, f)


if __name__ == "__main__":
    main()

