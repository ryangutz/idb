import requests
import json
from pprint import pprint

base_url = "https://ghoapi.azureedge.net/api/"
#page for number of reported cases for each disease
#leprocy is number of new cases
disease_pages = {
        "Cholera":"WHS3_40",
        "Diphtheria":"WHS3_41",
        "Japanese encephalitis":"WHS3_42",
        "Pertussis":"WHS3_43",
        "Leprosy":"WHS3_45",
        "Tetanus":"WHS3_46",
        "Meningitis":"WHS3_47",
        "Malaria":"WHS3_48",
        "Poliomyelitis":"WHS3_49",
        "Yellow fever":"WHS3_50",
        "H5N1 influenza":"WHS3_51",
        "Plague":"WHS3_52",
        "Tuberculosis":"WHS3_522",
        "Mumps":"WHS3_53",
        "Rubella":"WHS3_57",
        "Measles":"WHS3_62"
        }



#id
#name
#causes
#prevention
#spread type
#symptoms
#locations
#origin
#avg cost of treatment
#highest concentrated region
#death rate

def get_diseases():
    disease_lookups=[]
    for disease in disease_pages:
        page = disease_pages[disease]
        print(disease)
        year_updated={}
        cases_reported={}
        url=base_url+page
        data = requests.get(url).json()["value"]
        for item in data:
            if item["SpatialDimType"]!="COUNTRY" or item["TimeDimType"]!="YEAR":
                continue
            country_code = item["SpatialDim"]
            year = item["TimeDim"]
            cases = item["NumericValue"]
            if country_code not in cases_reported or year>year_updated[country_code]:
                cases_reported[country_code] = cases
                year_updated[country_code] = year
        lookup = (disease,cases_reported)
        print(cases_reported)
        disease_lookups.append(lookup)
    return disease_lookups


def main():
    url = "https://ghoapi.azureedge.net/api/DIMENSION/COUNTRY/DimensionValues"
    result = requests.get(url).json()
    country_data = result["value"]
    diseases = []
    disease_tables = get_diseases()
    i = 0
    for entry in disease_pages:
        disease={}
        disease["id"] = i
        i+=1
        disease["name"] = entry
        page = disease_pages[entry]

        ### PLACEHOLDERS ###
        disease["causes"] = "cause 1, cause 2"
        disease["prevention"] = "prevention"
        disease["spread_type"] = "spread"
        disease["symptoms"] = "symptom 1, symptom 2, symptom 3"
        disease["locations"] = "loc 1, loc 2, loc 3"
        disease["origin"] = "location"
        disease["avg_treatment_cost"] = 100
        disease["highest_concentrated_region"] = "region"
        disease["death_rate"] = 0.6
        ####################

        diseases.append(disease)
    json_diseases = {"data": diseases}
    with open("diseases.json", "w") as f:
        json.dump(json_diseases, f)


if __name__ == "__main__":
    main()

