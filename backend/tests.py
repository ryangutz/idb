import requests
import json
import unittest

url = "https://api.thehealthodyssey.com"

class ApiTests(unittest.TestCase):
	# ---------
	# GET Tests
	# ---------

	# Test locations
	def test1(self):
		response = requests.request("GET", url + '/api')
		assert response.ok

	# Test location
	def test2(self):
		response = requests.request("GET", url + '/api/locations')
		assert response.ok

	def test3(self):
		response = requests.request("GET", url + '/api/locations/China')
		assert response.ok

	def test4(self):
		response = requests.request("GET", url + '/api/locations?name=Can')
		assert response.ok
		response = requests.request("GET", url + '/api/locations?sort=name:asc')
		assert response.ok
		response = requests.request("GET", url + '/api/locations?search=Can')
		assert response.ok

	# Test diseases
	def test5(self):
		response = requests.request("GET", url + '/api/diseases')
		assert response.ok

	def test6(self):
		response = requests.request("GET", url + '/api/diseases/Cholera')
		assert response.ok

	def test7(self):
		response = requests.request("GET", url + '/api/diseases?name=Can')
		assert response.ok
		response = requests.request("GET", url + '/api/diseases?sort=name:asc')
		assert response.ok
		response = requests.request("GET", url + '/api/diseases?search=Can')
		assert response.ok

	# Test causes
	def test8(self):
		response = requests.request("GET", url + '/api/causes')
		assert response.ok

	def test9(self):
		response = requests.request("GET", url + '/api/causes/Corynebacterium diphtheriae')
		assert response.ok

	def test10(self):
		response = requests.request("GET", url + '/api/causes?name=Can')
		assert response.ok
		response = requests.request("GET", url + '/api/causes?sort=name:asc')
		assert response.ok
		response = requests.request("GET", url + '/api/causes?search=Can')
		assert response.ok

if __name__ == "__main__":
	unittest.main()