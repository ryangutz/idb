import flask
import flask_sqlalchemy
import flask_restless
from flask_cors import CORS

# -------------
# Configure APP
# -------------
app = flask.Flask(__name__)
app.config["DEBUG"] = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "mysql+pymysql://admin:password@pandoras-box.cwprv9ng32rh.us-east-2.rds.amazonaws.com:3306/tho_testing"

db = flask_sqlalchemy.SQLAlchemy(app)
db.drop_all()
CORS(app, resources={r"/api/*": {"origins": "*"}})

# ------
# MODELS
# ------

# --------
# Location
# --------

Location_Columns = {
    "attributes": ['name', 'region', 'flag', 'population', 'number_of_diseases', 'hospitals_per_100k', 'avg_age', 'mortality_rate', 'service_coverage_index', 'preventable_deaths'],
    "relations": [["causes", "cause_name"], ["diseases", "disease_name"]]
}

class Location(db.Model):
    __tablename__ = "Locations"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True, nullable=False)
    region = db.Column(db.String(30))
    population = db.Column(db.Integer)
    hospitals_per_100k = db.Column(db.Float)
    avg_age = db.Column(db.Float)
    mortality_rate = db.Column(db.Float)
    service_coverage_index = db.Column(db.Float)
    number_of_diseases = db.Column(db.Integer)
    causes = db.relationship('LocCauseRelations', lazy='joined')
    diseases = db.relationship('DisLocRelations', lazy='joined')
    flag = db.Column(db.String(200), unique=True)
    preventable_deaths = db.Column(db.Float)

# --------
# Diseases
# --------

Disease_Columns = {
    "attributes": ['name', 'image', 'death_rate', 'highest_concentrated_region'],
    "relations": [["causes", "cause_name"], ["symptoms", "symptom_name"], ["transmission", "trans_name"], ["treatment", "treatment_name"], ["locations", "location_name"]]
}

class Disease(db.Model):
    __tablename__ = "Diseases"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True, nullable=False)
    causes = db.relationship('CauseDisRelations', lazy='joined')
    image = db.Column(db.String(200), unique=True)
    symptoms = db.relationship('DisSympRelations', lazy='joined')
    treatment = db.relationship('DisTreatRelations', lazy='joined')
    transmission = db.relationship('DisTransRelations', lazy='joined')
    death_rate = db.Column(db.String(100), unique=True)
    locations = db.relationship('DisLocRelations', lazy='joined')
    highest_concentrated_region = db.Column(db.String(20))

# ------
# Causes
# ------

Cause_Columns = {
    "attributes": ['name', 'image', 'reaction_time'],
    "relations": [["disease", "disease_name"], ["treatment", "treatment_name"], ["transmission", "trans_name"], ["locations", "location_name"]]
}

class Cause(db.Model):
    __tablename__ = "Causes"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True, nullable=False)
    disease = db.relationship('CauseDisRelations', lazy='joined')
    treatment = db.relationship('CauseTreatRelations', lazy='joined')
    reaction_time = db.Column(db.String(30))
    transmission = db.relationship('CauseTransRelations', lazy='joined')
    image = db.Column(db.String(200), unique=True)
    locations = db.relationship('LocCauseRelations', lazy='joined')

# -----------
# Information
# -----------

class Transmission(db.Model):
    __tablename__ = "Transmissions"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True, nullable=False)

class Symptom(db.Model):
    __tablename__ = "Symptoms"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True, nullable=False)

class Treatment(db.Model):
    __tablename__ = "Treatments"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True, nullable=False)

# -------------
# Relationships
# -------------

class CauseTransRelations(db.Model):
    __tablename__ = "CauseTransRelations"
    id = db.Column(db.Integer, primary_key=True)
    cause_id = db.Column(db.Integer, db.ForeignKey('Causes.id'))
    trans_id = db.Column(db.Integer, db.ForeignKey('Transmissions.id'))
    cause_name = db.Column(db.String(100), nullable=False)
    trans_name = db.Column(db.String(100), nullable=False)
    __table_args__ = (db.UniqueConstraint('cause_id', 'trans_id', name='cause_trans_combo'), )

class LocCauseRelations(db.Model):
    __tablename__ = "LocCauseRelations"
    id = db.Column(db.Integer, primary_key=True)
    location_id = db.Column(db.Integer, db.ForeignKey('Locations.id'))
    cause_id = db.Column(db.Integer, db.ForeignKey('Causes.id'))
    location_name = db.Column(db.String(100), nullable=False)
    cause_name = db.Column(db.String(100), nullable=False)
    __table_args__ = (db.UniqueConstraint('location_id', 'cause_id', name='loc_cause_combo'), )

class CauseTreatRelations(db.Model):
    __tablename__ = "CauseTreatRelations"
    id = db.Column(db.Integer, primary_key=True)
    cause_id = db.Column(db.Integer, db.ForeignKey('Causes.id'))
    treatment_id = db.Column(db.Integer, db.ForeignKey('Treatments.id'))
    cause_name = db.Column(db.String(100), nullable=False)
    treatment_name = db.Column(db.String(100), nullable=False)
    __table_args__ = (db.UniqueConstraint('cause_id', 'treatment_id', name='cause_treat_combo'), )

class CauseDisRelations(db.Model):
    __tablename__ = "CauseDisRelations"
    id = db.Column(db.Integer, primary_key=True)
    cause_id = db.Column(db.Integer, db.ForeignKey('Causes.id'))
    disease_id = db.Column(db.Integer, db.ForeignKey('Diseases.id'))
    cause_name = db.Column(db.String(100), nullable=False)
    disease_name = db.Column(db.String(100), nullable=False)
    __table_args__ = (db.UniqueConstraint('cause_id', 'disease_id', name='cause_disease_combo'), )

class DisTransRelations(db.Model):
    __tablename__ = "DisTransRelations"
    id = db.Column(db.Integer, primary_key=True)
    disease_id = db.Column(db.Integer, db.ForeignKey('Diseases.id'))
    trans_id = db.Column(db.Integer, db.ForeignKey('Treatments.id'))
    disease_name = db.Column(db.String(100), nullable=False)
    trans_name = db.Column(db.String(100), nullable=False)
    __table_args__ = (db.UniqueConstraint('disease_id', 'trans_id', name='disease_treat_combo'), )

class DisLocRelations(db.Model):
    __tablename__ = "DisLocRelations"
    id = db.Column(db.Integer, primary_key=True)
    disease_id = db.Column(db.Integer, db.ForeignKey('Diseases.id'))
    location_id = db.Column(db.Integer, db.ForeignKey('Locations.id'))
    disease_name = db.Column(db.String(100), nullable=False)
    location_name = db.Column(db.String(100), nullable=False)
    num_cases = db.Column(db.Integer)
    __table_args__ = (db.UniqueConstraint('disease_id', 'location_id', name='disease_location_combo'), )

class DisTreatRelations(db.Model):
    __tablename__ = "DisTreatRelations"
    id = db.Column(db.Integer, primary_key=True)
    disease_id = db.Column(db.Integer, db.ForeignKey('Diseases.id'))
    treatment_id = db.Column(db.Integer, db.ForeignKey('Treatments.id'))
    disease_name = db.Column(db.String(100), nullable=False)
    treatment_name = db.Column(db.String(100), nullable=False)
    __table_args__ = (db.UniqueConstraint('disease_id', 'treatment_id', name='disease_treat_combo'), )

class DisSympRelations(db.Model):
    __tablename__ = "DisSympRelations"
    id = db.Column(db.Integer, primary_key=True)
    disease_id = db.Column(db.Integer, db.ForeignKey('Diseases.id'))
    symptom_id = db.Column(db.Integer, db.ForeignKey('Symptoms.id'))
    disease_name = db.Column(db.String(100), nullable=False)
    symptom_name = db.Column(db.String(100), nullable=False)
    __table_args__ = (db.UniqueConstraint('disease_id', 'symptom_id', name='disease_treat_combo'), )

# ---------------
# Create database
# ---------------

db.create_all()
