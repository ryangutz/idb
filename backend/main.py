import flask
import flask_sqlalchemy
import flask_restless
from models import *
import json
import requests
import itertools

# Create Flask-Restless API manager and Flask-Restful api.
manager = flask_restless.APIManager(app, flask_sqlalchemy_db=db)

# ----
# CORS
# ----

@app.after_request
def apply_caching(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Credentials'] = 'true'
    return response

# --------------
# Query Builders
# --------------

# Build json string for filtering
def build_filter(field, op, val):
    val = '%'+val+'%'
    return {u'name': field, u'op': op, u'val': val}

def build_outer_filter(field, op, val):
    return {u'name': field, u'op': op, u'val': val}

# Build json string for sorting
def build_sort(field, direction):
    return {u'field': field, u'direction': direction}

# Build search query
def get_columns():
    if "locationsapi0.locationsapi" == flask.request.endpoint:
        return Location_Columns
    elif "diseasesapi0.diseasesapi" == flask.request.endpoint:
        return Disease_Columns
    elif "causesapi0.causesapi" == flask.request.endpoint:
        return Cause_Columns

def build_search(val):
    query = []
    columns = get_columns()

    # Build search query
    for field in columns["attributes"]:
        query.append(build_filter(field, "ilike", val))
    for field in columns["relations"]:
        query.append(build_outer_filter(field[0], "any", build_filter(field[1], "ilike", val)))
    return query

# Add queries to request
def build_queries(search_params=None, **kw):
    filters  = []
    searches = []
    sorts    = []
    for i in flask.request.args.to_dict(): 
        if i == 'q':
            break 
        else :
            # Build sort queries
            if str(i) == 'sort':
                if ':' not in flask.request.args[i]:
                    sorts.append(build_sort(flask.request.args[i], "asc"))
                else:
                    sort_args = flask.request.args[i].split(':')
                    sorts.append(build_sort(sort_args[0], sort_args[1]))
            # Build search queries
            elif str(i) == 'search':
                searches += build_search(flask.request.args[i])
            # Build operator queries
            elif '<' in str(i):
                filters.append(build_outer_filter(str(i)[:-1], "le", flask.request.args[i]))
            elif '>' in str(i):
                filters.append(build_outer_filter(str(i)[:-1], "ge", flask.request.args[i]))
            # Build filter queries
            elif str(i) != 'page' and str(i) != 'results_per_page':
                columns = get_columns()
                if str(i) in columns["attributes"]:
                    filters.append(build_filter(str(i), "ilike", flask.request.args[i]))
                else:
                    for field in columns["relations"]:
                        if field[0] == str(i):
                            filters.append(build_outer_filter(field[0], "any", build_filter(field[1], "ilike", flask.request.args[i])))
                    
    
    if len(searches) > 0:
            filters.append({u'or': searches})
    if len(filters) > 0:
        search_params['filters'] = filters
    if len(sorts) > 0:
        search_params['order_by'] = sorts

# -----------
# Serializers
# -----------

def get_relationships():
    if "locationsapi0.locationsapi" == flask.request.endpoint:
        return Location_Columns['relations']
    elif "diseasesapi0.diseasesapi" == flask.request.endpoint:
        return Disease_Columns['relations']
    elif "causesapi0.causesapi" == flask.request.endpoint:
        return Cause_Columns['relations']

def single_serializer(result=None, **kw):
    relations = get_relationships()
    # Format correctly
    for relation in relations:
        result[relation[0]] = [item[relation[1]] for item in result[relation[0]]]

def many_serializer(result=None, **kw):
    relations = get_relationships()
    # Format correctly
    for entry in result['objects']:
        for relation in relations:
            entry[relation[0]] = [item[relation[1]] for item in entry[relation[0]]]

# ---------
# Endpoints
# ---------
# ---------
# Locations
# ---------

# Create api/locations endpoint
manager.create_api(
    Location,
    collection_name = "locations",
    results_per_page=0,
    primary_key = "name",
    methods = ["GET"],
    preprocessors = {
        'GET_MANY': [build_queries]
    },
    postprocessors = {
        'GET_SINGLE': [single_serializer],
        'GET_MANY': [many_serializer]
    }
)

# --------
# Diseases
# --------

# Create api/diseases endpoint
manager.create_api(
    Disease,
    collection_name = "diseases",
    results_per_page=0,
    primary_key = "name",
    methods = ["GET"],
    preprocessors = {
        'GET_MANY': [build_queries]
    },
    postprocessors = {
        'GET_SINGLE': [single_serializer],
        'GET_MANY': [many_serializer]
    }
)

# ------
# Causes
# ------

# Create api/causes endpoint
manager.create_api(
    Cause,
    collection_name = "causes",
    results_per_page=0,
    primary_key = "name",
    methods = ["GET"],
    preprocessors = {
        'GET_MANY': [build_queries]
    },
    postprocessors = {
        'GET_SINGLE': [single_serializer],
        'GET_MANY': [many_serializer]
    }
)

# ----
# Home
# ----

@app.route('/', methods=['GET'])
def home():
    return "Hello World!"

@app.route("/api", methods=['GET'])
def database():
    if flask.request.method == 'GET':
        arguments = '?' + flask.request.query_string
        locations = requests.request('GET', 'https://api.thehealthodyssey.com/api/locations' + arguments)
        diseases = requests.request('GET', 'https://api.thehealthodyssey.com/api/diseases'+ arguments)
        causes = requests.request('GET', 'https://api.thehealthodyssey.com/api/causes'+ arguments)

        results = {
            "locations" : locations.json(),
            "diseases": diseases.json(),
            "causes": causes.json()
        }
        return flask.jsonify(**results)
    else:
        return "Hello World!"

# Test
@app.route("/api/test")
def test():
    filters  = []
    searches = []
    sorts    = []
    for i in flask.request.args.to_dict(): 
        if i == 'q':
            break 
        else :
            # Build sort queries
            if str(i) == 'sort':
                if ':' not in flask.request.args[i]:
                    sorts.append(build_sort(flask.request.args[i], "asc"))
                else:
                    sort_args = flask.request.args[i].split(':')
                    sorts.append(build_sort(sort_args[0], sort_args[1]))
            # Build search queries
            elif str(i) == 'search':
                searches += build_search(flask.request.args[i])
            # Build operator queries
            elif '<' in str(i):
                filters.append(build_outer_filter(str(i)[:-1], "le", flask.request.args[i]))
            # Build filter queries
            elif str(i) != 'page' and str(i) != 'results_per_page':
                filters.append(build_filter(str(i), "ilike", flask.request.args[i]))

    queries = {
        "filters": filters,
        "searches": searches,
        "sorts": sorts
    }

    return flask.jsonify(**queries)

app.run("0.0.0.0")
