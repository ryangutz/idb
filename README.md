# The Health Odyssey

Group members: <br />

| name           | eid      | gitlab id      |
| -------------- | -------- | -------------- |
| Ryan Gutierrez | rbg672   | ryangutz       |
| Chase Heath    | cph794   | ChaseHeath     |
| Tae Kim        | tk22334  | ktae           |
| Megan Mealey   | mnm2788  | mmealey        |
| Simon Pinochet | sp44454  | simon.pinochet |

Git SHA: 812b831365795ddbe46362bd5d562a7ddac1b2fd

Project leader: Ryan Gutierrez

Gitlab pipelines: n/a

Website Link: https://thehealthodyssey.com/

Estimated completion time: <br />
Ryan: 15 <br />
Chase: 12 <br />
Tae: 13 <br />
Megan: 12 <br />
Simon: 15 <br />

Actual completion time: <br />
Ryan: 17 <br />
Chase 15 <br />
Tae: 15 <br />
Megan: 14 <br />
Simon: 14 <br />

Comments:
