import requests
import json

base_url = "https://ghoapi.azureedge.net/api/"
#page for number of reported cases for each disease
#leprocy is number of new cases
disease_pages = {
        "Cholera":"WHS3_40",
        "Diphtheria":"WHS3_41",
        "Japanese encephalitis":"WHS3_42",
        "Pertussis":"WHS3_43",
        "Leprosy":"WHS3_45",
        "Tetanus":"WHS3_46",
        "Meningitis":"WHS3_47",
        "Malaria":"WHS3_48",
        "Poliomyelitis":"WHS3_49",
        "Yellow fever":"WHS3_50",
        "Plague":"WHS3_52",
        "Tuberculosis":"TB_e_inc_num",
        "Mumps":"WHS3_53",
        "Rubella":"WHS3_57",
        "Measles":"WHS3_62"
        }


wikidata="https://www.wikidata.org/wiki/Special:EntityData/"
wiki_pages = {
        "Cholera":"Q12090",
        "Diphtheria":"Q134649",
        "Japanese encephalitis":"Q738292",
        "Pertussis":"Q134859",
        "Leprosy":"Q36956",
        "Tetanus":"Q47790",
        "Meningitis":"Q48143",
        "Malaria":"Q12156",
        "Poliomyelitis":"Q12195",
        "Yellow fever":"Q154874",
        "Plague":"Q133780",
        "Tuberculosis":"Q12204",
        "Mumps":"Q176741",
        "Rubella":"Q155857",
        "Measles":"Q79793"
        }
#need to add .json to end

#get country names from codes
country_info_url = "https://ghoapi.azureedge.net/api/DIMENSION/COUNTRY/DimensionValues"
country_data = requests.get(country_info_url).json()["value"]
ccode_lookup = {}
for entry in country_data:
    ccode = entry["Code"]
    name = entry["Title"]
    ccode_lookup[ccode]=name
    region_name = entry["ParentTitle"]
    region_code = entry["ParentCode"]
    ccode_lookup[region_code] = region_name


def get_name_wikidata(id_str):
    data=requests.get(wikidata+id_str+".json").json()["entities"][id_str]
    name = data["labels"]["en"]["value"]
    return name

def get_property_list(prop,claims):
    if prop not in claims:
        return "no data"
    item_list = []
    for item in claims[prop]:
        item_id = item["mainsnak"]["datavalue"]["value"]["id"]
        item_list.append(get_name_wikidata(item_id))
    if len(item_list)<1:
        return "no data"
    return item_list

def get_quantity(prop,claims,ind=0):
    if prop not in claims:
        return "no data"
    item = claims[prop][0]
    value=item["mainsnak"]["datavalue"]["value"]["amount"]
    return str(value)

def get_info(disease):
    info={}
    causes="no data"
    img="no data"
    symptoms="no data"
    disease_page = wikidata+wiki_pages[disease]+".json"
    data=requests.get(disease_page).json()["entities"][wiki_pages[disease]]
    claims = data["claims"]
    if "P1478" in claims:
        causes=claims["P1478"]
    elif "P828" in claims:
        causes=claims["P828"]

    cause_names=[]
    if causes=="no data":
        info["causes"]="no data"
    else:
        for cause in causes:
            cause_id=cause["mainsnak"]["datavalue"]["value"]["id"]
            name = get_name_wikidata(cause_id)
            if name!="infection":
                cause_names.append(name)
    if len(cause_names)<1:
        info["cause"] = "no data"
    else:
        info["cause"] = cause_names

    if "P18" in claims:
        img="https://commons.wikimedia.org/wiki/File:"+claims["P18"][0]["mainsnak"]["datavalue"]["value"]
    info["image"] = img

    symptom_list=[]
    if "P780" in claims:
        for symptom in claims["P780"]:
            symptom_id=symptom["mainsnak"]["datavalue"]["value"]["id"]
            symptom_list.append(get_name_wikidata(symptom_id))
        info["symptoms"] = symptom_list
    else:
        info["symptoms"] = "no data"
    info["treatments"] = get_property_list("P924",claims)
    info["spread"] = get_property_list("P1060",claims)
    info["deaths"] = get_quantity("P1120",claims)

    return info

def get_locations(disease):
    disease_page = disease_pages[disease]
    data = requests.get(base_url+disease_page).json()["value"]
    year_updated={}
    cases_reported={}
    cases_by_region={}
    for item in data:
        if item["TimeDimType"]!="YEAR":
            continue
        if item["SpatialDim"] not in ccode_lookup:
            continue
        loc_code=ccode_lookup[item["SpatialDim"]]
        year = item["TimeDim"]
        try:
            cases = int(float(item["Value"].split(" ")[0]))
        except ValueError:
            cases = item["NumericValue"]
        if item["SpatialDimType"]=="COUNTRY":
            if loc_code not in cases_reported or year>year_updated[loc_code]:
                cases_reported[loc_code] = cases
                year_updated[loc_code] = year
        elif item["SpatialDimType"]=="REGION" and item["SpatialDim"]!="GLOBAL":
            if loc_code not in cases_by_region or year>year_updated[loc_code]:
                cases_by_region[loc_code] = cases
                year_updated[loc_code] = year
    locations=[]
    for location in cases_reported:
        if cases_reported[location]!=None and cases_reported[location]>0:
            locations.append(location)
    for info in cases_by_region:
        if cases_by_region[info]==None:
            cases_by_region[info]=0
    regions=[]
    if cases_by_region.values()!=None and len(cases_by_region.values())>1:
        region_max = max(cases_by_region.values())
        for region in cases_by_region:
            if cases_by_region[region]==region_max:
                regions.append(region)
    if len(regions)<1:
        regions="no data"
    return locations,regions



#id
#name
#causes
#prevention
#spread type
#symptoms
#locations
#origin
#avg cost of treatment
#highest concentrated region
#death rate

def get_diseases():
    disease_lookups=[]
    for disease in disease_pages:
        page = disease_pages[disease]
        year_updated={}
        cases_reported={}
        url=base_url+page
        data = requests.get(url).json()["value"]
        for item in data:
            if item["SpatialDimType"]!="COUNTRY" or item["TimeDimType"]!="YEAR":
                continue
            country_code = item["SpatialDim"]
            year = item["TimeDim"]
            cases = item["NumericValue"]
            if country_code not in cases_reported or year>year_updated[country_code]:
                cases_reported[country_code] = cases
                year_updated[country_code] = year
        lookup = (disease,cases_reported)
        disease_lookups.append(lookup)
    return disease_lookups


def main():
    url = "https://ghoapi.azureedge.net/api/DIMENSION/COUNTRY/DimensionValues"
    #result = requests.get(url).json()
    #country_data = result["value"]
    diseases = []
    i = 0
    for entry in disease_pages:
        print(entry, flush=True)
        disease={}
        disease["id"] = i
        i+=1
        disease["name"] = entry
        page = disease_pages[entry]
        wiki_info = get_info(entry)
        disease["causes"] = wiki_info["cause"]
        disease["image"] = wiki_info["image"]
        disease["symptoms"] = wiki_info["symptoms"]
        disease["treatment"] = wiki_info["treatments"]
        disease["spread_type"] = wiki_info["spread"]
        disease["death_rate"] = wiki_info["deaths"]

        locations,regions = get_locations(entry)
        disease["locations"] = locations
        print(disease["causes"], flush=True)
        if regions!=None:
            disease["highest_concentrated_region"] = regions[0]

        diseases.append(disease)
    json_diseases = {"data": diseases}
    with open("diseases.json", "w") as f:
        json.dump(json_diseases, f)


if __name__ == "__main__":
    main()
