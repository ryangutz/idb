import requests
import json

who_url="https://ghoapi.azureedge.net/api/"
disease_pages = {
        "Cholera":"WHS3_40",
        "Diphtheria":"WHS3_41",
        "Japanese encephalitis":"WHS3_42",
        "Pertussis":"WHS3_43",
        "Leprosy":"WHS3_45",
        "Tetanus":"WHS3_46",
        "Meningitis":"WHS3_47",
        "Malaria":"WHS3_48",
        "Poliomyelitis":"WHS3_49",
        "Yellow fever":"WHS3_50",
        "Plague":"WHS3_52",
        "Tuberculosis":"TB_e_inc_num",
        "Mumps":"WHS3_53",
        "Rubella":"WHS3_57",
        "Measles":"WHS3_62"
        }
wikidata="https://www.wikidata.org/wiki/Special:EntityData/"
wiki_pages = {
        "Cholera":"Q12090",
        "Diphtheria":"Q134649",
        "Japanese encephalitis":"Q738292",
        "Pertussis":"Q134859",
        "Leprosy":"Q36956",
        "Tetanus":"Q47790",
        "Meningitis":"Q48143",
        "Malaria":"Q12156",
        "Poliomyelitis":"Q12195",
        "Yellow fever":"Q154874",
        "Plague":"Q133780",
        "Tuberculosis":"Q12204",
        "Mumps":"Q176741",
        "Rubella":"Q155857",
        "Measles":"Q79793"
        }

country_info_url = "https://ghoapi.azureedge.net/api/DIMENSION/COUNTRY/DimensionValues"
country_data = requests.get(country_info_url).json()["value"]
ccode_lookup = {}
for entry in country_data:
    ccode = entry["Code"]
    name = entry["Title"]
    ccode_lookup[ccode]=name

def main():
    causes_list=[]
    i=0
    for disease in disease_pages:
        print("\n")
        print(disease)
        disease_info = get_info(disease)
        causes = disease_info["cause"]
        cause_id_lookup=disease_info["cause_ids"]
        disease_locations=get_locations(disease)
        for c in causes:
            if c=="unknown":
                continue
            print(c)
            cause={}
            cause["name"] = c
            cause["disease"]=disease
            cause["treatment"]=disease_info["treatments"]
            cause["related_causes"]="none"
            if len(disease_info["cause"])>1:
                cause["related_causes"] = [item for item in disease_info["cause"] if item!=c]
            cause["reaction_time"]="no data"
            if disease_info["min_days"]!=None and disease_info["max_days"]!=None:
                cause["reaction_time"]=disease_info["min_days"] + " to " + disease_info["max_days"]
            cause["transmission"]=disease_info["spread"]
            cause["image"]=get_img_wikidata(cause_id_lookup[c])
            cause["locations"] = disease_locations
            causes_list.append(cause)

    json_causes = {"data": causes_list}
    with open("causes.json", "w") as f:
        json.dump(json_causes, f)




def get_name_wikidata(id_str):
    data=requests.get(wikidata+id_str+".json").json()["entities"][id_str]
    name = data["labels"]["en"]["value"]
    return name

def get_img_wikidata(id_str):
    claims=requests.get(wikidata+id_str+".json").json()["entities"][id_str]["claims"]
    if "P18" in claims:
        return "https://commons.wikimedia.org/wiki/File:"+claims["P18"][0]["mainsnak"]["datavalue"]["value"]
    else:
        return "image not found"

def get_property_list(prop,claims):
    if prop not in claims:
        return ["no data"]
    item_list = []
    for item in claims[prop]:
        item_id = item["mainsnak"]["datavalue"]["value"]["id"]
        item_list.append(get_name_wikidata(item_id))
    if len(item_list)<1:
        return ["no data"]
    return item_list
#    return ", ".join(item_list)

def get_quantity(prop,claims,ind=0):
    if prop not in claims:
        return "no data"
    item = claims[prop][0]
    value=item["mainsnak"]["datavalue"]["value"]["amount"]
    return str(value)

def get_info(disease):
    info={}
    causes="no data"
    img="no data"
    symptoms="no data"
    disease_page = wikidata+wiki_pages[disease]+".json"
    data=requests.get(disease_page).json()["entities"][wiki_pages[disease]]
    claims = data["claims"]
    if "P1478" in claims:
        causes=claims["P1478"]
    elif "P828" in claims:
        causes=claims["P828"]

    cause_names=[]
    cause_ids={}
    if causes=="no data":
        info["causes"]="no data"
    else:
        for cause in causes:
            cause_id=cause["mainsnak"]["datavalue"]["value"]["id"]
            name = get_name_wikidata(cause_id)
            cause_names.append(name)
            cause_ids[name]=cause_id
    info["cause_ids"]=cause_ids
    if len(cause_names)<1:
        info["cause"] = ["unknown"]
    else:
        info["cause"] = cause_names


    info["treatments"] = get_property_list("P924",claims)
    info["spread"] = get_property_list("P1060",claims)
    info["deaths"] = get_quantity("P1120",claims)
    info["min_days"] = get_quantity("P3488",claims)
    info["max_days"] = get_quantity("P3487",claims)

    return info

def get_locations(disease):
    disease_page = disease_pages[disease]
    data = requests.get(who_url+disease_page).json()["value"]
    year_updated={}
    cases_reported={}
    for item in data:
        if item["TimeDimType"]!="YEAR" or item["SpatialDimType"]!="COUNTRY":
            continue
        loc_code=ccode_lookup[item["SpatialDim"]]
        year = item["TimeDim"]
        cases = item["NumericValue"]
        if cases==None:
            cases=0.0
        if loc_code not in cases_reported or year>year_updated[loc_code]:
                cases_reported[loc_code] = cases
                year_updated[loc_code] = year
    locations=[]
    for location in cases_reported:
        if cases_reported[location]!=None and cases_reported[location]>0:
            locations.append(location)
    return locations


if __name__ == "__main__":
    main()
