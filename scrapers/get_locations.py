import requests
import json
import math

who_url="https://ghoapi.azureedge.net/api/"
base_url = "https://ghoapi.azureedge.net/api/"
disease_pages = {
        "Cholera":"WHS3_40",
        "Diphtheria":"WHS3_41",
        "Japanese encephalitis":"WHS3_42",
        "Pertussis":"WHS3_43",
        "Leprosy":"WHS3_45",
        "Tetanus":"WHS3_46",
        "Meningitis":"WHS3_47",
        "Malaria":"WHS3_48",
        "Poliomyelitis":"WHS3_49",
        "Yellow fever":"WHS3_50",
        "Plague":"WHS3_52",
        "Tuberculosis":"TB_e_inc_num",
        "Mumps":"WHS3_53",
        "Rubella":"WHS3_57",
        "Measles":"WHS3_62"
        }

wikidata="https://www.wikidata.org/wiki/Special:EntityData/"
wiki_pages = {
        "Cholera":"Q12090",
        "Diphtheria":"Q134649",
        "Japanese encephalitis":"Q738292",
        "Pertussis":"Q134859",
        "Leprosy":"Q36956",
        "Tetanus":"Q47790",
        "Meningitis":"Q48143",
        "Malaria":"Q12156",
        "Poliomyelitis":"Q12195",
        "Yellow fever":"Q154874",
        "Plague":"Q133780",
        "Tuberculosis":"Q12204",
        "Mumps":"Q176741",
        "Rubella":"Q155857",
        "Measles":"Q79793"
        }
#need to add .json to end

#get country names from codes
country_info_url = "https://ghoapi.azureedge.net/api/DIMENSION/COUNTRY/DimensionValues"
country_data = requests.get(country_info_url).json()["value"]
ccode_lookup = {}
for entry in country_data:
    ccode = entry["Code"]
    name = entry["Title"]
    ccode_lookup[ccode]=name
    region_name = entry["ParentTitle"]
    region_code = entry["ParentCode"]
    ccode_lookup[region_code] = region_name

def get_name_wikidata(id_str):
    data=requests.get(wikidata+id_str+".json").json()["entities"][id_str]
    name = data["labels"]["en"]["value"]
    return name

cause_cache={}
def get_cause(disease):
    if disease in cause_cache:
        return cause_cache[disease]
    causes="no data"
    disease_page = wikidata+wiki_pages[disease]+".json"
    data=requests.get(disease_page).json()["entities"][wiki_pages[disease]]
    claims = data["claims"]
    if "P1478" in claims:
        causes=claims["P1478"]
    elif "P828" in claims:
        causes=claims["P828"]
    if causes=="no data":
        return "no data"
    cause_names=[]
    for cause in causes:
        cause_id=cause["mainsnak"]["datavalue"]["value"]["id"]
        name = get_name_wikidata(cause_id)
        cause_names.append(name)
    if len(cause_names)<1:
        return ""
    cause_entry = cause_names
    cause_cache[disease] = cause_entry
    return cause_entry

def get_diseases():
    disease_lookups=[]
    for disease in disease_pages:
        page = disease_pages[disease]
        year_updated={}
        cases_reported={}
        url=base_url+page
        data = requests.get(url).json()["value"]
        for item in data:
            if item["SpatialDimType"]!="COUNTRY" or item["TimeDimType"]!="YEAR":
                continue
            country_code = item["SpatialDim"]
            year = item["TimeDim"]
            cases = item["NumericValue"]
            try:
                cases = int(float(item["Value"].split(" ")[0]))
            except ValueError:
                cases = item["NumericValue"]
            if country_code not in cases_reported or year>year_updated[country_code]:
                cases_reported[country_code] = cases
                year_updated[country_code] = year
        lookup = (disease,cases_reported)
        disease_lookups.append(lookup)
    return disease_lookups

def get_populations():
    year_updated={}
    population={}
    url="https://ghoapi.azureedge.net/api/RS_1845"
    data = requests.get(url).json()["value"]
    for item in data:
        if item["SpatialDimType"]!="COUNTRY" or item["TimeDimType"]!="YEAR":
            continue
        country_code = item["SpatialDim"]
        year = item["TimeDim"]
        pop = item["NumericValue"]
        if country_code not in population or year>year_updated[country_code]:
            population[country_code] = pop
            year_updated[country_code] = year
    return (population,year_updated)

def get_hospital_density():
    year_updated={}
    hospitals={}
    url="https://ghoapi.azureedge.net/api/DEVICES00"
    data = requests.get(url).json()["value"]
    for item in data:
        if item["SpatialDimType"]!="COUNTRY" or item["TimeDimType"]!="YEAR":
            continue
        country_code = item["SpatialDim"]
        year = item["TimeDim"]
        hosp = item["NumericValue"]
        if country_code not in hospitals or year>year_updated[country_code]:
            hospitals[country_code] = hosp
            year_updated[country_code] = year
    return hospitals

def get_median_age():
    year_updated={}
    ages={}
    url="https://ghoapi.azureedge.net/api/WHS9_88"
    data = requests.get(url).json()["value"]
    for item in data:
        if item["SpatialDimType"]!="COUNTRY" or item["TimeDimType"]!="YEAR":
            continue
        country_code = item["SpatialDim"]
        year = item["TimeDim"]
        age = item["NumericValue"]
        if country_code not in ages or year>year_updated[country_code]:
            ages[country_code] = age
            year_updated[country_code] = year
    return ages

def get_mort():
    year_updated={}
    rates={}
    url="https://ghoapi.azureedge.net/api/WHS9_CDR?$filter=SpatialDimType%20eq%20%27COUNTRY%27"
    data = requests.get(url).json()["value"]
    for item in data:
        if item["SpatialDimType"]!="COUNTRY" or item["TimeDimType"]!="YEAR":
            continue
        country_code = item["SpatialDim"]
        year = item["TimeDim"]
        rate = item["NumericValue"] * 100
        if country_code not in rates or year>year_updated[country_code]:
            rates[country_code] = rate
            year_updated[country_code] = year
    return rates


def get_bordering_countries(ccode):
    if len(ccode)!=3:
        return []
    src_url = "https://restcountries.eu/rest/v2/alpha/" + ccode
    data=requests.get(src_url).json()
    if "borders" in data:
        output_list=[]
        for c in data["borders"]:
            if c in ccode_lookup:
                output_list.append(ccode_lookup[c])
        return output_list
    else:
        return ["no data"]


def get_flag(ccode):
    if len(ccode)!=3:
        return []
    src_url = "https://restcountries.eu/rest/v2/alpha/" + ccode
    data=requests.get(src_url).json()
    if "flag" in data:
        return data["flag"]
    else:
        return []


def get_scis(): #SCI = service coverage index, percentage of people that have access to treatment for infectious disease. Max is 80
    year_updated={}
    scis={}
    url = "https://ghoapi.azureedge.net/api/UHC_SCI_INFECT"
    data = requests.get(url).json()["value"]
    for item in data:
        if item["SpatialDimType"]!="COUNTRY" or item["TimeDimType"]!="YEAR":
            continue
        country_code = item["SpatialDim"]
        year = item["TimeDim"]
        sci = item["NumericValue"]
        if country_code not in scis or year>year_updated[country_code]:
            scis[country_code] = sci
            year_updated[country_code] = year
    return scis


def get_death_rates():
    year_updated={}
    cause_dict = {}
    url="https://ghoapi.azureedge.net/api/MORT_400?$filter=Dim1%20eq%20%27BTSX%27%20and%20Dim2%20eq%20%27AGEAll%27"
    data = requests.get(url).json()["value"]
    for item in data:
        location = item["SpatialDim"].split("_")
        if len(location) != 2 or location[0]!="REG6":
            continue
        region_code = location[1]
        year=item["TimeDim"]
        cause=item["Dim3"]
        rate = item["NumericValue"]
        if region_code not in cause_dict or year>year_updated[region_code]:
            year_updated[region_code]=year
            new_cause_dict={}
            new_cause_dict[cause]=rate
            cause_dict[region_code]=new_cause_dict
        elif region_code in cause_dict and year==year_updated[region_code]:
            region_dict = cause_dict[region_code]
            region_dict[cause] = rate
    return cause_dict

def get_disease_rates(r_dict):
    total_rate=0.0
    disease_keys = ["GHE001","GHE002", "GHE003", "GHE004", "GHE005", "GHE029", "GHE018", "GHE019", "GHE020", "GHE021","GHE022","GHE023","GHE012","GHE013","GHE014","GHE015","GHE016","GHE017","GHE011"]
    for key in disease_keys:
        if key in r_dict:
            total_rate+=r_dict[key]
    return total_rate

def get_preventable_deaths():
    prev_deaths = {}
    with open("annual-number-of-deaths-by-cause.csv" ) as f:
        disease_inds = [6,7,11,14,16,22,27,28,29]
        next_line = f.readline()
        while next_line:
            info = next_line.split(",")
            next_line = f.readline()
            ccode=info[1]
            if ccode=="" or '_' in ccode or info[2]!="2016":
                continue
            deaths=[]
            for num in info[3:]:
                if '>' in num:
                    num=num[1:]
                num="0"+num
                deaths.append(math.ceil(float(num)))
            disease_deaths = sum([deaths[i] for i in disease_inds])
            total_deaths = sum(deaths)
            prev_death_perc = (disease_deaths/total_deaths)*100
            prev_deaths[ccode] = prev_death_perc
    return prev_deaths


def main():
    url = "https://ghoapi.azureedge.net/api/DIMENSION/COUNTRY/DimensionValues"
    result = requests.get(url).json()
    country_data = result["value"]
    countries = []
    population,pop_year = get_populations()
    disease_tables = get_diseases()
    print(disease_tables)
    hospital_density = get_hospital_density()
    med_ages = get_median_age()
    mortality_rates = get_mort()
    sci_info = get_scis()
    regional_death_causes = get_death_rates()
    prev_deaths = get_preventable_deaths()
    i = 0
    for entry in country_data:
        print(entry["Title"])
        country={}
        ccode = entry["Code"]
        country["code"] = ccode
        country["id"] = i
        i+=1
        country["name"] = entry["Title"]
        if country["name"] == "SPATIAL_SYNONYM":
            continue
        country["region"] = entry["ParentTitle"]
        region_code = entry["ParentCode"]
        if region_code not in regional_death_causes:
            country["disease_mort_rate"] = "no data"
        else:
            region_death_dict = regional_death_causes[region_code]
            country["disease_mort_rate"] = get_disease_rates(region_death_dict)
        if ccode in population:
            country["population"] = population[ccode]
        else:
            country["population"] = "no data"
        if ccode in hospital_density:
            country["hospitals_per_100k"] = hospital_density[ccode]
        else:
            country["hospitals_per_100k"] = "no data"

        if ccode in med_ages:
            country["avg_age"] = med_ages[ccode]
        else:
            country["avg_age"] = "no data"

        if ccode in mortality_rates:
            country["mortality_rate"] = mortality_rates[ccode]
        else:
            country["mortality_rate"] = "no data"
        
        if ccode in sci_info:
            country["service_coverage_index"] = sci_info[ccode]
        else:
            country["service_coverage_index"] = None
        
        diseases = []
        causes = []
        cases = []
        for disease, entry in disease_tables:
            if ccode in entry:
                reported_cases = entry[ccode]
                if reported_cases!=None and reported_cases>0:
                    for x in get_cause(disease):
                        causes.append(x)
                    diseases.append(disease)
                    cases.append(reported_cases)
        country["number_of_diseases"] = len(diseases)
        if len(diseases)>0:
            country["cause_names"] = causes
            country["diseases"] = diseases
            country["disease_cases_per_year"] = cases
        else:
            country["cause_names"] = "no data"
            country["diseases"] = "no data"
            country["disease_cases_per_year"] = "no data"
        country["bordering_countries"]=get_bordering_countries(ccode)
        country["flag"] = get_flag(ccode)

        if ccode in prev_deaths:
            country["preventable_deaths"] = prev_deaths[ccode]
        else:
            country["preventable_deaths"] = "no data"

        countries.append(country)
    json_countries = {"data": countries}
    with open("locations.json", "w") as f:
        json.dump(json_countries, f)


if __name__ == "__main__":
    main()
