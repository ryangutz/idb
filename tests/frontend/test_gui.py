import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By


class TestGUI(unittest.TestCase):
    def setUp(self):
        # for docker
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.driver = webdriver.Chrome(options=chrome_options)
        # for local testing
        # self.driver = webdriver.Safari()
        self.driver.implicitly_wait(10)
        self.driver.get("https://www.thehealthodyssey.com")

    def test_title(self):
        home_page_name = "THO"
        self.assertIn(home_page_name, self.driver.title)
    
    def test_homepage_links(self):
        a = []
        a = self.driver.find_elements_by_tag_name("a")
        assert len(a) > 0
    
    def test_homepage_pictures(self):
        a = []
        a = self.driver.find_elements_by_tag_name("img")
        assert len(a) > 0
    
    def test_navbar(self):
        a = []
        a = self.driver.find_elements_by_class_name("nav-link")
        assert len(a) > 0

    def test_diseases_instances(self):
        self.driver.get("https://www.thehealthodyssey.com/Diseases")
        a = []
        a = self.driver.find_elements_by_partial_link_text("See")
        assert len(a) > 0

    def test_causes_instances(self):
        self.driver.get("https://www.thehealthodyssey.com/Causes")
        a = []
        a = self.driver.find_elements_by_tag_name("tr")
        assert len(a) > 0

    def test_locations_instances(self):
        self.driver.get("https://www.thehealthodyssey.com/Locations")
        a = []
        a = self.driver.find_elements_by_tag_name("tr")
        assert len(a) > 0

    def test_about_page_links(self):
        self.driver.get("https://www.thehealthodyssey.com/about")
        a = []
        a = self.driver.find_elements_by_tag_name("a")
        assert len(a) > 0
    
    def test_about_page_profile(self):
        self.driver.get("https://www.thehealthodyssey.com/about")
        assert self.driver.find_element_by_id('Ryan Gutierrez') != None
        assert self.driver.find_element_by_id('Chase Heath') != None
        assert self.driver.find_element_by_id('Simon Pinochet') != None
        assert self.driver.find_element_by_id('Megan Mealey') != None
        assert self.driver.find_element_by_id('Tae Kim') != None
    
    def test_about_page_pictures(self):
        self.driver.get("https://www.thehealthodyssey.com/about")
        a = []
        a = self.driver.find_elements_by_tag_name("img")
        assert len(a) >= 5

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":  # pragma: no cover
    unittest.main()
